# Thermal and optical conductivity
Here is the code used for the computations for [Thermal and optical conductivity in the Holstein model at half filling and at finite temperature in the Luttinger-liquid and charge-density-wave regime](https://arxiv.org/abs/2304.13193).
The source files are in the "src" directory and the scripts to run them are in "scripts" directory. The Makefile will need to be adapted depending on the working directory.
## The repository dependes on the following other rerpositories

<ul>
 <li> [https://github.com/jansendavid/ITensor](https://github.com/jansendavid/ITensor): branch developer </li>
 <li> [https://gitlab.gwdg.de/jansen32/tensorft](https://gitlab.gwdg.de/jansen32/tensorft): for finite termpeature calculations, branch master </li>
 <li> [https://gitlab.gwdg.de/jansen32/tensortimeev](https://gitlab.gwdg.de/jansen32/tensortimeev): for time evolution methods, branch master </li>
  <li> [https://gitlab.gwdg.de/jansen32/tensortools](https://gitlab.gwdg.de/jansen32/tensortools): for the Holstein site sets and more, branch master </li>
    <li> [https://gitlab.gwdg.de/jansen32/parallel_tdvp_lbo](https://gitlab.gwdg.de/jansen32/parallel_tdvp_lbo): for parallel TDVP, branch master </li>
<ul>
