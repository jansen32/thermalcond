# pragma once
#define EIGEN_USE_MKL_ALL
#include"basis.hpp"
#include<cmath>
#include <Eigen/Sparse>
#include <type_traits>
#include"operators.hpp"
#ifdef MOM
using ValType= std::complex<double>; 
#else
using ValType= double;
#endif
namespace Operators{
 template<class TotalBasis, class SubBasis>
 Mat Elecnr_localOperator(const TotalBasis& totalBasis, const SubBasis& subBasis, size_t i,const double omega=1.)

  {
 
	      using LeftBasisIt= typename TotalBasis::LeftBasisIt;     
using Lattice_L=typename SubBasis::Lattice;     
    size_t dim=totalBasis.dim;

    Mat op(dim, dim);
    //   op.setZero();
  


      for(const auto& tpState : totalBasis)
	{
	  	       LeftBasisIt it2=subBasis.find(LeftId(tpState));	     

	     Lattice_L state_L=GetLattice(*it2);

	  // left vs right id ?
	       op.coeffRef(Position(tpState), Position(tpState))+=(omega*state_L[i]);
       
	   
	  
	}
      return op;
      
       }
  template<class TotalBasis, class SubBasis_L, class SubBasis_R>
  Mat CurCoupOperator_loc(const TotalBasis& totalBasis, const SubBasis_L& subBasis_L,const SubBasis_R& subBasis_R, size_t i, size_t j,double var=1. )
  {// electron current with phonon displacement for Holstein current
	      using LeftBasisIt= typename TotalBasis::LeftBasisIt;     
     using RightBasisIt= typename TotalBasis::RightBasisIt;


using Lattice_L=typename SubBasis_L::Lattice;     
using Lattice_R=typename SubBasis_R::Lattice;     
    
 
   size_t dim=totalBasis.dim;
    size_t sites=totalBasis.sites;
    Mat op(dim, dim);
 	   for( auto& tpState : totalBasis)
 	     {
	     



	       LeftBasisIt it2=subBasis_L.find(LeftId(tpState));	     

	       Lattice_L state_L=GetLattice(*it2);


	   	    		    if(state_L[i]==state_L[j])
   	       	     {
	  // 	     
    		  
   	   	     }
    	   	    else{
		      

		      Lattice_L temp_L=state_L;
		      
		       state_L.setPartNr(j, temp_L[i]);
		      state_L.setPartNr(i, temp_L[j]);
		     		 
	  	     size_t signControl=CheckSign(temp_L, i, j);
		       
		     it2= subBasis_L.find(state_L.GetId());
		     
	  	       RightBasisIt it3=totalBasis.rbasis.find(RightId(tpState));
  	    size_t particleNumber=subBasis_R.particlesAt(RightId(tpState), j);
		      double otherSign=(state_L[j]==1)?+1:-1;
// 	     // J_jb^{dagger}_j
  	     if( particleNumber==subBasis_R.maxParticles)
  	       {
  	       }
	     else{

	       Lattice_R state_R(GetLattice(*it3));
	     state_R.setPartNr(j, particleNumber+1);


  	       RightBasisIt it4= subBasis_R.find(state_R.GetId());
		     
	  	       size_t newStateNr= Position(*it4)*totalBasis.lbasis.dim +Position(*it2);

	   	              if(signControl%2==0)
   	   	    	 {
	   		   op.coeffRef(newStateNr, Position(tpState))-= otherSign*ValType{var*(std::sqrt(state_R[j]))};
			 }
   	   	       else
   	   	    	 { op.coeffRef(newStateNr, Position(tpState))+=otherSign*ValType{var*(std::sqrt(state_R[j]))};
	     }		    
}
// // // 	     // J_jb_j

  	     if( particleNumber==0)
  	       {
  	       }
	     else{

	       Lattice_R state_R(GetLattice(*it3));
	     state_R.setPartNr(j, particleNumber-1);


  	       RightBasisIt it4= subBasis_R.find(state_R.GetId());
		     
	  	       size_t newStateNr= Position(*it4)*totalBasis.lbasis.dim +Position(*it2);

	   	              if(signControl%2==0)
   	   	    	 {
	   		   op.coeffRef(newStateNr, Position(tpState))-= otherSign*ValType{var*(std::sqrt(state_R[j]+1))};
			 }
   	   	       else
   	   	    	 { op.coeffRef(newStateNr, Position(tpState))+=otherSign*ValType{var*(std::sqrt(state_R[j]+1))};
	     }		    
}




  	   	     }
 	      	     

 }
 		    return op;
 }

  template<class TotalBasis, class SubBasis >
  Mat EKinLocalOperator(const TotalBasis& totalBasis, const SubBasis& subBasis,size_t i, size_t j, double var=1.)
     {


       using LeftBasisIt= typename TotalBasis::LeftBasisIt;     
     using RightBasisIt= typename TotalBasis::RightBasisIt;
     using Lattice=typename SubBasis::Lattice;     
     size_t dim=totalBasis.dim;
     size_t sites=totalBasis.sites;
     Mat op(dim, dim);      
	   for( auto& tpState : totalBasis)
	     {

	       	   
	       LeftBasisIt it2=subBasis.find(LeftId(tpState));	     

	       Lattice state=GetLattice(*it2);

	   	    		    if(state[i]==state[j])
   	       	     {
	  // 	     
    		  
   	   	     }
   	   	    else{
		      

		      Lattice temp=state;
		      
		       state.setPartNr(j, temp[i]);
		      state.setPartNr(i, temp[j]);
		     		 
	  	     size_t signControl=CheckSign(temp, i, j);
		       
		     it2= subBasis.find(state.GetId());
		     
	  	       RightBasisIt it3=totalBasis.rbasis.find(RightId(tpState));
		     
	  	       size_t newStateNr= Position(*it3)*totalBasis.lbasis.dim +Position(*it2);
		     
	   	              if(signControl%2==0)
   	   	    	 {
	   		   op.coeffRef(newStateNr, Position(tpState))-= ValType{var};}
   	   	       else
   	   	    	 { op.coeffRef(newStateNr, Position(tpState))+= ValType{var};}
		    }
	     }

	   	     
 	      	     
	   

   	  
    
  return op;
  }

  template<class TotalBasis, class SubBasis >
  Mat CurrLocalOperator(const TotalBasis& totalBasis, const SubBasis& subBasis,size_t i, size_t j, double var=1.)
     {


       using LeftBasisIt= typename TotalBasis::LeftBasisIt;     
     using RightBasisIt= typename TotalBasis::RightBasisIt;
     using Lattice=typename SubBasis::Lattice;     
     size_t dim=totalBasis.dim;
     size_t sites=totalBasis.sites;
     Mat op(dim, dim);      
	   for( auto& tpState : totalBasis)
	     {

	       	   
	       LeftBasisIt it2=subBasis.find(LeftId(tpState));	     

	       Lattice state=GetLattice(*it2);

	   	    		    if(state[i]==state[j])
   	       	     {
	  // 	     
    		  
   	   	     }
   	   	    else{
		      

		      Lattice temp=state;
		      
		       state.setPartNr(j, temp[i]);
		      state.setPartNr(i, temp[j]);
		     		 
	  	     size_t signControl=CheckSign(temp, i, j);
		       
		     it2= subBasis.find(state.GetId());
		     
	  	       RightBasisIt it3=totalBasis.rbasis.find(RightId(tpState));
		     
	  	       size_t newStateNr= Position(*it3)*totalBasis.lbasis.dim +Position(*it2);
		     
		      double otherSign=(state[j]==1)?+1:-1;
	   	              if(signControl%2==0)
   	   	    	 {
			   
			   op.coeffRef(newStateNr, Position(tpState))-= otherSign*ValType{var};
}
	   		   
   	   	       else
   	   	    	 { 

			   op.coeffRef(newStateNr, Position(tpState))+= otherSign*ValType{var};
}
		    }

	   	     }
 	      	     
	   

   	  
    
  return op;
  }

}
