#include "itensor/all.h"
#include"files.hpp"
#include"holstein.hpp"
#include <iostream>
#include <iomanip>
#include<utility>
#include <boost/program_options.hpp>
#include"write_data.h"
#include"tdvp.hpp"
#include <boost/filesystem.hpp>

using namespace std;
using namespace itensor;
void print_state(MPS& psi)
{
   		       for(int j=1; j<length(psi); j++)
    			 {
    			   auto ind_K=rightLinkIndex(psi,j);
			   std::cout<< "("<<j<<","<<j+1<<")"<< " dim : "<<dim(ind_K);
			   std::cout<<" "<<std::endl;
}
			   std::cout<<std::endl;
			   return;}
int main(int argc, char *argv[])
{
  
  double cutoff{};

  int M{};

  int Maxd{};
int Mind{};

  int L{};

std::string mpsName{};
 std::string siteSetName{};
  double t0{};
  double omega{};
  double gamma{};
  double dt{};
  double tot{};

  std::string DIR{};

  std::string stot{};


  std::string filename="";
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("mpsN", boost::program_options::value(&mpsName)->default_value("noName"), "mpsN")
      ("siteN", boost::program_options::value(&siteSetName)->default_value("noName"), "siteSetName")
      ("L", boost::program_options::value(&L)->default_value(4), "L")
      ("M", boost::program_options::value(&M)->default_value(4), "M")
      ("Maxd", boost::program_options::value(&Maxd)->default_value(200), "Maxd")
      ("Mind", boost::program_options::value(&Mind)->default_value(200), "Mind")
      ("t0", boost::program_options::value(&t0)->default_value(1.0), "t0")
      ("omg", boost::program_options::value(&omega)->default_value(1.0), "omg")
      ("gam", boost::program_options::value(&gamma)->default_value(1.0), "gam")
      ("dt", boost::program_options::value(&dt)->default_value(0.01), "dt")
      ("tot", boost::program_options::value(&tot)->default_value(1.0), "tot")
      ("d", boost::program_options::value(&DIR)->default_value("NONAME"), "d")
      ("cut", boost::program_options::value(&cutoff)->default_value(1E-15), "cut");

    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    
    if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("L"))
      {      std::cout << "L: " << vm["L"].as<int>() << '\n';

      	

      }
   
      	     if (vm.count("M"))
      {      std::cout << "M: " << vm["M"].as<int>() << '\n';
      
      
      }
	           	     if (vm.count("Maxd"))
      {      std::cout << "Maxd: " << vm["Maxd"].as<int>() << '\n';
      
      
      }
			     if (vm.count("Mind"))
      {      std::cout << "Mind: " << vm["Mind"].as<int>() << '\n';
      
      
      }

      	 if (vm.count("t0"))
      {      std::cout << "t0: " << vm["t0"].as<double>() << '\n';
      
      
      }
	 if (vm.count("gam"))
      {      std::cout << "gamma: " << gamma << '\n';
      
      
      }
      	 	 if (vm.count("omg"))
      {      std::cout << "omega: " << vm["omg"].as<double>() << '\n';
      
      
      }
      		 if (vm.count("tot"))
      {      std::cout << "tot: " << vm["tot"].as<double>() << '\n';
      	stot="tot"+std::to_string(vm["tot"].as<double>()).substr(0, 3);
      
      }
      		 if (vm.count("dt"))
      {      std::cout << "dt: " << vm["dt"].as<double>() << '\n';
      
      
      }
      		 if (vm.count("cut"))
      {      std::cout << "cutoff: " << vm["cut"].as<double>() << '\n';


      }

      }
    }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }
  using Holstein_exp = MixedSiteSet<HolsteinSite_down,HolsteinSite_up>;
   auto sites = readFromFile<Holstein_exp>(siteSetName);
    auto psi = readFromFile<MPS>(mpsName);

    print_state(psi);
   


  auto argsMPS = itensor::Args("Cutoff=",cutoff,"MinDim=",Mind,"MaxDim=",Maxd,"Truncate", false,"DoNormalize",false, "SVDMethod", "gesdd","NumCenter",1,"Silent",true);



   std::chrono::duration<double> elapsed_seconds;
 std::chrono::time_point<std::chrono::system_clock> start, end;
   

    auto sweeps = Sweeps(1);
    sweeps.maxdim() = Maxd;
    sweeps.mindim() = Mind;
    sweeps.cutoff() = cutoff;
    sweeps.niter() = 10;
    println(sweeps);
    auto sweeps_1=sweeps;
    auto sweeps_2=sweeps;
    AutoMPO Ecurr{};

      Ecurr=makeECurr_staggered_FT(sites, t0, gamma, omega);

    auto ECurr = toMPO(Ecurr);
    auto am=makeHolstHam_FT_staggeredAux(sites, t0, gamma, omega);
    auto H1 = toMPO(am);
auto H2 = toMPO(am);
    auto argsObs= itensor::Args("Method=","DensityMatrix","MinDim=",Mind,"MaxDim=",Maxd,"Cutoff=",cutoff);
	      auto y1 = applyMPO(ECurr,psi,argsObs);

	      y1.noPrime();
	     
auto psi1=y1;
  print_state(psi1);
  auto psi2=y1;

    double lbomax=0;
   std::string obsname="JeJe";
     write_data_FT(psi1, psi2, lbomax, lbomax, 0, argsObs, DIR,obsname, argsMPS);
     std::string t_start="0.0000";
     write_state_bds( psi1, psi2, t_start,  DIR);

    auto  t_step=-0.5*dt*Cplx_i;
    TDVP<MPS> tdvp1(psi1,H1, t_step, sweeps_1,argsMPS);
    TDVP<MPS> tdvp2(psi2,H2,-t_step,sweeps_2,argsMPS);

    //    std::cout<< "start energy "<<innerC(psi1,H1, psi1)<<std::endl;
    //	      	      std::cout<< "start obs "<<innerC(psi2,Curr, psi)<<std::endl;
	 
 
 
				std::vector<double> en_vec_1={real(innerC(psi1,H1, psi1))};
				std::vector<double> en_vec_2=en_vec_1;

			   auto vecSize=en_vec_1.size();
    			   
			itensor::ToFile(en_vec_1, DIR+"/energy_1.dat", vecSize);
			   itensor::ToFile(en_vec_2, DIR+"/energy_2.dat", vecSize);

       auto N_e =AutoMPO(sites);
       for(int j=1; j<length(psi1); j+=2)
	 {
	   N_e+=1,"n", j;
} 
       auto N_e_MPO=toMPO(N_e);
       std::cout<<"N e "<< innerC(psi1,N_e_MPO, psi1)/innerC(psi1,psi1)<<std::endl;


	      int i=1;
	      double t_in=0.;

      	   while(t_in + i*dt<=tot)
      {
       

	      
	      tdvp1.step_new(sweeps_1);
	      
	      	      tdvp2.step_new(sweeps_2);

if(i==1)
	  {
	    std::string t_now=std::to_string(i*dt).substr(0,4);
	    write_state_bds(psi1, psi2,  t_now,DIR);

  }
	      	       write_data_FT(psi1, psi2, lbomax , lbomax, t_in +i*dt, argsObs, DIR, obsname, argsMPS);
		    

		      i++;
      	   	     }
		 en_vec_1={real(innerC(psi1,H1, psi1))};
				en_vec_2={real(innerC(psi2,H2, psi2))};

			    vecSize=en_vec_1.size();
    			   
			itensor::ToFile(en_vec_1, DIR+"/energy_1.dat", vecSize);
			   itensor::ToFile(en_vec_2, DIR+"/energy_2.dat", vecSize);
	   bool apply_op=true;
	   write_state(psi1,psi2, mpsName, mpsName,  DIR,stot,apply_op,apply_op);
  return 0;
}
