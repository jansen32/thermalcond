#include<iostream>
#define EIGEN_USE_MKL_ALL
#include"basis.hpp"
#include"operators.hpp"
#include"diag.h"
#include"thermal_edops.hpp"
#include"files.hpp"
#include"timeev.hpp"
#include"ETH.hpp"
#include"tpoperators.hpp"
#include <boost/program_options.hpp>

double L(freq, eta, dE)
{
  return (eta)/((freq-dE)*(freq-dE)+eta*eta);
}
int main(int argc, char *argv[])
{
  using namespace Eigen;
using namespace std;
using namespace Many_Body;
  using Fermi_HubbardBasis= TensorProduct<ElectronBasis, ElectronBasis>;
   using HolsteinBasis= TensorProduct<ElectronBasis, PhononBasis>;
  using Mat= Operators::Mat;
  using boost::program_options::value;
   size_t M{};
  size_t L{};
  int Ne{};
  double eta{};
  double dom{}
  double om_max{};

  bool PB{};
  std::string dirname="";
  
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("L", value(&L)->default_value(4), "L")
      ("M,m", value(&M)->default_value(2), "M")
      ("Ne", value(&Ne)->default_value(1), "ne")
      ("eta", value(&eta)->default_value(1), "eta")
      ("dom", value(&dom)->default_value(1), "dom")
      ("om_max", value(&om_max)->default_value(1.), "om_max")
      ("dir", value(&dirname)->default_value(""), "dirname");
  


    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);

  if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("L"))
      {      std::cout << "L: " << vm["L"].as<size_t>() << '\n';

      }
     if (vm.count("M"))
      {
	std::cout << "M: " << vm["M"].as<size_t>() << '\n';

      }
         if (vm.count("Ne"))
      {      std::cout << "Ne: " << vm["Ne"].as<int>() << '\n';

      }
       
    }
  }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }




  ElectronBasis e( L, Ne);

  PhononBasis ph(L, M);

      HolsteinBasis TP(e, ph);
 
	 Eigen::MatrixXcd comm_J=Eigen::MatrixXcd::Zero(TP.dim, TP.dim);    
	 Eigen::MatrixXcd comm_Je=Eigen::MatrixXcd::Zero(TP.dim, TP.dim);        
    
	 Eigen::MatrixXcd JJ=Eigen::MatrixXcd::Zero(TP.dim, TP.dim);        
	 Eigen::MatrixXcd JeJe=Eigen::MatrixXcd::Zero(TP.dim, TP.dim);        
	  Eigen::VectorXd EV=Eigen::VectorXd::Zero(TP.dim);        
	  Eigen::VectorXd EK=Eigen::VectorXd::Zero(TP.dim);        

       	   bin_read(dirname+"/comm_Jmat.bin", comm_J);
       	   bin_read(dirname+"/comm_Jemat.bin", comm_Je);
       	   bin_read(dirname+"/JJmat.bin", JJ);
       	   bin_read(dirname+"/JeJemat.bin", JeJe);
   	     bin_read(dirname+"/E.bin", EV);
   	     bin_read(dirname+"/EKdiag.bin", EK);
	     std::vector<double> Ts;
	     std::vector<double> omega;
	     Ts.push_back(0.);
	     //	     std::cout<< JJ<<std::endl;
	     //std::cout<< JeJe<<std::endl;
	     for(double d=0; i<=om_max;d+=dom )
	       {
		 
  omega.push_back(d);
}
	     for(int i=1; i<=40;i++ )
	       {
		 double T=0.1*i;
  Ts.push_back(T);
}
	     std::vector<double> Z(Ts.size(), 0);
	     std::vector<std::complex<double>> valsJJ(Ts.size(), 0);
	     std::vector<std::complex<double>> valsJeJe(Ts.size(), 0);

	     std::vector<std::complex<double>> vals_EK(Ts.size(),0);

	     std::vector<std::complex<double>> vals_JeComm(Ts.size(), 0);
	     std::vector<std::complex<double>> vals_JComm(Ts.size(), 0);
	     std::vector<std::complex<double>> rest_J(Ts.size(), 0);
	     std::vector<std::complex<double>> rest_Je(Ts.size(), 0);


	     //	     std::cout<< EK<<std::endl;
	     std::cout<<std::endl;
	     // std::cout<< comm_J.diagonal()<<std::endl;

	     for(int j=0; j<JJ.rows(); j++) //
	       {
		 //	 		 std::cout<< "E "<< EV[j] << " and "<<JJ(j,j)<<std::endl;
				 if(j>0)
				   {
				     valsJJ[0]+=(JJ(j,0)*std::conj(JJ(j,0))/(EV[j]-EV[0]))*2.;
				     valsJeJe[0]+=(JeJe(j,0)*std::conj(JeJe(j,0))/(EV[j]-EV[0]))*2.;
}
    for(int m=1; m<Ts.size();m++)
      {

	Z[m]+=std::exp(-(EV[j]-EV[0])/Ts[m]);
	valsJJ[m]+=std::exp(-(EV[j]-EV[0])/Ts[m])*JJ(j,j)*std::conj(JJ(j,j))/(2*Ts[m]*L);
	valsJeJe[m]+=std::exp(-(EV[j]-EV[0])/Ts[m])*JeJe(j,j)*std::conj(JeJe(j,j))/(2*Ts[m]*Ts[m]*L);
	vals_EK[m]+=std::exp(-(EV[j]-EV[0])/Ts[m])*EK(j);

	vals_JeComm[m]+=std::exp(-(EV[j]-EV[0])/Ts[m])*comm_Je(j,j);
	vals_JComm[m]+=std::exp(-(EV[j]-EV[0])/Ts[m])*comm_J(j,j);
}

for(int l=0; l<JJ.rows(); l++)
  {

   if(std::abs(EV[j]-EV[l])< 1.0e-10)
    {
      //      std::cout<< "degenrate "<< EV[j]<<std::endl;
	          for(int m=1; m<Ts.size();m++)
      {

	valsJJ[m]+=std::exp(-(EV[j]-EV[0])/Ts[m])*JJ(j,l)*std::conj(JJ(j,l))/(2*Ts[m]*L);
	valsJeJe[m]+=std::exp(-(EV[j]-EV[0])/Ts[m])*JeJe(j,l)*std::conj(JeJe(j,l))/(2*Ts[m]*Ts[m]*L);
	//valsJJ[m]+=std::exp(-(EV[j]-EV[0])/Ts[m])*JJ(l,j)*std::conj(JJ(l,j))/(2*Ts[m]*L);
	//valsJeJe[m]+=std::exp(-(EV[j]-EV[0])/Ts[m])*JeJe(l,j)*std::conj(JeJe(l,j))/(2*Ts[m]*Ts[m]*L);
}

}
   else{
	          for(int m=1; m<Ts.size();m++)
      {
     rest_J[m]+=2*std::exp(-(EV[j]-EV[0])/Ts[m])*JJ(j,l)*std::conj(JJ(j,l))/(EV[l]-EV[j]);
     rest_Je[m]+=2*std::exp(-(EV[j]-EV[0])/Ts[m])*JeJe(j,l)*std::conj(JeJe(j,l))/(EV[l]-EV[j]);
      }
}


}
 }
	     std::cout<<comm_J(0,0)<<"   "<<comm_Je(0,0)<<std::endl;
	     vals_EK[0]=EK(0);
   	          for(int m=0; m<Ts.size();m++)
      {
	if(m>1){
	valsJJ[m]/=Z[m];
	valsJeJe[m]/=Z[m];
	vals_EK[m]/=Z[m];
	vals_JComm[m]/=Z[m];
	vals_JeComm[m]/=Z[m];
	rest_J[m]/=Z[m];
	rest_Je[m]/=Z[m];
	}
	std::cout<< " Ts "<<Ts[m]<< "  " <<valsJJ[m]<< "  " << valsJeJe[m]<< " sum rule Charge " <<	vals_JComm[m] << " and  " << vals_EK[m]<<std::endl;
	std::cout<< "charge "<<vals_JComm[m]<< " and "<< rest_J[m]<<std::endl;
	std::cout<< "energy "<<vals_JeComm[m]<< " and "<< rest_Je[m]<<std::endl;
       }
		  
   	     bin_write(dirname+"/vals_JeComm.bin", vals_JeComm);
   	     bin_write(dirname+"/vals_JComm.bin", vals_JeComm);
   	     bin_write(dirname+"/T.bin", Ts);
   	     bin_write(dirname+"/valsJJ.bin", valsJJ);
   	     bin_write(dirname+"/valsJeJe.bin", valsJeJe);
   	     bin_write(dirname+"/rest_J.bin", rest_J);
   	     bin_write(dirname+"/rest_Je.bin", rest_Je);
   	     bin_write(dirname+"/vals_EK.bin", vals_EK);


  return 0;
}
 
