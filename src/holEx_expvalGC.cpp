#include<iostream>
#define EIGEN_USE_MKL_ALL
#include"basis.hpp"
#include"operators.hpp"
#include"diag.h"
#include"thermal_edops.hpp"
#include"files.hpp"
#include"timeev.hpp"
#include"ETH.hpp"
#include"tpoperators.hpp"
#include <boost/program_options.hpp>
int main(int argc, char *argv[])
{
  using namespace Eigen;
using namespace std;
using namespace Many_Body;
  using Fermi_HubbardBasis= TensorProduct<ElectronBasis, ElectronBasis>;
   using HolsteinBasis= TensorProduct<ElectronBasis, PhononBasis>;
  using Mat= Operators::Mat;
  using boost::program_options::value;
   size_t M{};
  size_t L{};
  double T{};
  double t0{};
  double omega{};
  double gamma{};

  bool PB{};
  std::string dirname="";
  std::string filename={};
  
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("L", value(&L)->default_value(4), "L")
      ("M,m", value(&M)->default_value(2), "M")
      ("T", value(&T)->default_value(1), "T")
      ("t0", value(&t0)->default_value(1.), "t0")
      ("gam", value(&gamma)->default_value(1.), "gamma")
      ("omg", value(&omega)->default_value(1.), "omega")
      ("dir", value(&dirname)->default_value(""), "dirname")
    ("pb", value(&PB)->default_value(true), "PB");
  


    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);

  if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("L"))
      {      std::cout << "L: " << vm["L"].as<size_t>() << '\n';
	filename+="L"+std::to_string(vm["L"].as<size_t>());
      }
     if (vm.count("M"))
      {
	std::cout << "M: " << vm["M"].as<size_t>() << '\n';
	filename+="M"+std::to_string(vm["M"].as<size_t>());
      }
         if (vm.count("T"))
      {      std::cout << "T: " << vm["T"].as<double>() << '\n';


      }

      if (vm.count("t0"))
      {
	std::cout << "t0: " << vm["t0"].as<double>() << '\n';
	filename+="t0"+std::to_string(vm["t0"].as<double>()).substr(0, 6);
      }
       if (vm.count("omg"))
      {
	std::cout << "omega: " << vm["omg"].as<double>() << '\n';
	filename+="omega"+std::to_string(vm["omg"].as<double>()).substr(0, 6);
      }

       if (vm.count("gam"))
      {
	std::cout << "gamma: " << vm["gam"].as<double>() << '\n';
	filename+="gam"+std::to_string(vm["gam"].as<double>()).substr(0, 6);
      }
       if (vm.count("pb"))
      {
	std::cout << "PB: " << vm["pb"].as<bool>() << '\n';
	filename+="PB"+std::to_string(vm["pb"].as<bool>());
      }
    }
  }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }
  double expval=0;
  double Z=0;

  for(int Ne=0; Ne<=L;Ne++)
    {

  ElectronBasis e( L, Ne);

  PhononBasis ph(L, M);
  //         std::cout<< ph<<std::endl;
      HolsteinBasis TP(e, ph);
 
 	
	   


 		std::cout<< "dim "<< TP.dim << std::endl;  


 		Mat E1=Operators::EKinOperatorL(TP, e, t0, PB);

 		Mat Ebdag=Operators::NBosonCOperator(TP, ph, gamma, PB);
 		Mat Eb=Operators::NBosonDOperator(TP, ph, gamma, PB);
 		Mat Eph=Operators::NumberOperator(TP, ph, omega,  PB);

		//-gam sun x_i /2
		double C=1;
		Mat Eextra=-gamma*0.5*((Operators::BosonDOperator_1(TP, ph,C,0,PB))+(Operators::BosonCOperator_1(TP, ph,C,0,PB)));
		for(int i=1; i<L;i++)
		  {
		    Eextra+=-gamma*0.5*((Operators::BosonDOperator_1(TP, ph,C,i,PB))+(Operators::BosonCOperator_1(TP, ph,C,i,PB)));
		  }

       Mat H=E1+Eph  +Ebdag + Eb+Eextra;
       std::vector<double> obs(TP.dim, Ne);
     Eigen::MatrixXd HH=Eigen::MatrixXd(H);
      Eigen::VectorXd eigenVals(TP.dim);
     	  Many_Body::diagMat(HH, eigenVals);
  std::vector<double> ensvec(TP.dim);
   for(int i=0; i<eigenVals.size(); i++)
     {

       expval+=std::exp(-eigenVals(i)/T)*Ne;
       Z+=std::exp(-eigenVals(i)/T);
     }

  std::cout<<"expval temp "<< expval<<std::endl;
    }


  std::cout<<"expval "<< expval/Z<<std::endl;
  return 0;
}
 
