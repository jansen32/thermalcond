#include "itensor/all.h"
#include"holstein.hpp"
#include "makebasis.hpp"
#include"files.hpp"
#include"tdvp.hpp"
#include <boost/program_options.hpp>
#include"tdvp_lbo.hpp"

using namespace itensor;
struct dbl_cmp {
    dbl_cmp(double v, double d) : val(v), delta(d) { }
    inline bool operator()(const double &x) const {
        return abs(x-val) < delta;
    }
  private:
    double val, delta;
};
 template<typename SITES>
 void write_densdenscor(MPS& psi,SITES& sites, double beta, std::string DIR  )
 {

     for(int i=1; i<=length(psi)-1; i+=2)
       {
	 std::vector<std::complex<double>> stateCor;
         psi.position(i);
	 auto psidag=dag(psi);
	 psidag.prime("Link");
	  auto niop = op(sites,"n",i);
	  auto ket=psi(i);
	  auto bra=dag(prime(ket, "Site"));

     stateCor.push_back(eltC(bra*niop*ket));
	 for(int j=i+2;j<=length(psi)-1; j+=2)
	   {

 auto njop = op(sites,"n",j);
 auto li_1=leftLinkIndex(psi,i);
 auto C=prime(psi(i), li_1)*niop;
 C*=prime(psidag(i), "Site");
 for(int k=i+1; k<j; ++k)
   {
     C*=psi(k);
     C*=psidag(k);

}
     auto lj=rightLinkIndex(psi,j);
     C*=prime(psi(j), lj)*njop;
     C*=prime(psidag(j), "Site");

     stateCor.push_back(eltC(C));
 }
  std::string SC="aT"+std::to_string(1./beta).substr(0, 6)+".bin";
     itensor::bin_write(DIR+"/DensDenssite"+std::to_string(i)+SC, stateCor);
       }
     return;
}
void print_state(MPS& psi)
{
   		       for(int j=1; j<length(psi); j++)
    			 {
    			   auto ind_K=rightLinkIndex(psi,j);
			   std::cout<< "("<<j<<","<<j+1<<")"<< " dim : "<<dim(ind_K);
			   std::cout<<" "<<std::endl;
}
			   std::cout<<std::endl;
			   return;}

int
main(int argc, char *argv[])
    {
     using boost::program_options::value;
   int M{};
   int L{};
   int n_e{};
   double T{};
   double dt{};
   double t0{};
   double omega{};
    int lboMd{};
    int Mind{};
    int Maxd{};
   double gamma{};
   double cutoff{};
 double lbocutoff{};
  std::string sM{};
  std::string sMaxd{};
  std::string sMind{};
  std::string sL{};
  std::string star{};
  std::string sT{};
  std::string st0{};
  std::string somega{};
  std::string sgamma{};
  std::string starget{};
  std::string scutoff{};


  std::string DIR{""};
  std::string sdt;
  std::string stot;
    std::string slboMd{};
    std::string slbocutoff{};
     bool saveAll{};
  std::string filename="FTdisptdvplbo";
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("L", value(&L)->default_value(4), "L")
      ("n_e", value(&n_e)->default_value(1), "n_e")
      ("T", value(&T)->default_value(0.1), "T")
      ("dt", value(&dt)->default_value(0.01), "dt")
      ("M,m", value(&M)->default_value(2), "M")
      ("Maxd", value(&Maxd)->default_value(3000), "Maxd")
      ("Mind", value(&Mind)->default_value(0), "Mind")
      ("t0", value(&t0)->default_value(1.), "t0")
      ("gam", value(&gamma)->default_value(1.), "gam")
      ("lboMd", boost::program_options::value(&lboMd)->default_value(100), "lboMd")
       ("lbocut", boost::program_options::value(&lbocutoff)->default_value(1E-9), "lbocut")
      ("cut", boost::program_options::value(&cutoff)->default_value(1E-9), "cut")
      ("omg", value(&omega)->default_value(1.), "omg")
      ("d", value(&DIR)->default_value("NoNAME"), "d")
      ("SA", boost::program_options::value(&saveAll)->default_value(0), "SA");


      

  

    
    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);

  if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("L"))
      {      std::cout << "L: " << vm["L"].as<int>() << '\n';
      	sL="L"+std::to_string(vm["L"].as<int>());
	filename+=sL;
      }
         if (vm.count("M"))
      {      std::cout << "M: " << vm["M"].as<int>() << '\n';
      	sM="M"+std::to_string(vm["M"].as<int>());
	filename+=sM;
      }
         if (vm.count("n_e"))
      {      std::cout << "n_e: " << vm["n_e"].as<int>() << '\n';

	filename+="n_e"+std::to_string(vm["n_e"].as<int>());;
      }
      	 if (vm.count("t0"))
      {      std::cout << "t0: " << vm["t0"].as<double>() << '\n';
      	st0="t0"+std::to_string(vm["t0"].as<double>()).substr(0, 5);
      	filename+=st0;
      }
      	 	 if (vm.count("omg"))
      {      std::cout << "omega: " << vm["omg"].as<double>() << '\n';
      	somega="omg"+std::to_string(vm["omg"].as<double>()).substr(0, 5);
      		filename+=somega;
      }

      		 if (vm.count("gam"))
      {      std::cout << "gamma: " << vm["gam"].as<double>() << '\n';
      	sgamma="gam"+std::to_string(vm["gam"].as<double>()).substr(0, 5);
      		filename+=sgamma;
      }
		 if (vm.count("T"))
      {      std::cout << "T: " << vm["T"].as<double>() << '\n';
      	stot="T"+std::to_string(vm["T"].as<double>()).substr(0, 5);
      		filename+=stot;
      }
		       		 if (vm.count("dt"))
      {      std::cout << "dt: " << vm["dt"].as<double>() << '\n';
      	sdt="dt"+std::to_string(vm["dt"].as<double>()).substr(0, 6);
      		filename+=sdt;
      }
		 if (vm.count("cut"))
      {      std::cout << "cutoff: " << vm["cut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["cut"].as<double>();
	 scutoff="cut"+ss.str();
      	filename+=scutoff;
      }
		       if (vm.count("Maxd"))
      {      std::cout << "Maxd: " << vm["Maxd"].as<int>() << '\n';
      	sMaxd="Maxd"+std::to_string(vm["Maxd"].as<int>());
	filename+=sMaxd;
      }
		       if (vm.count("Mind"))
      {      std::cout << "Mind: " << vm["Mind"].as<int>() << '\n';
      	sMind="Mind"+std::to_string(vm["Mind"].as<int>());
	filename+=sMind;
      }
		       if (vm.count("lboMd"))
		          {      std::cout << "lboMd: " << vm["lboMd"].as<int>() << '\n';
      	slboMd="lboMd"+std::to_string(vm["lboMd"].as<int>());
      	filename+=slboMd;
      }
		       if (vm.count("d"))
			 {      std::cout << "dirname: " << vm["d"].as<std::string>() << '\n';

      }
			  		 if (vm.count("lbocut"))
		   {
		std::cout << "lbocutoff: " << vm["lbocut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["lbocut"].as<double>();
	 slbocutoff="lbocut"+ss.str();
      	filename+=slbocutoff;
      }
      }
    }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }
  filename+=".bin";
     

  double beta=1./T;
  int N=2*L;
     std::vector<int> v(N, M);

       using Holstein_exp = MixedSiteSet<HolsteinSite_down,HolsteinSite_up>;
        auto sites = Holstein_exp(N,{"ConserveNf=",true,
                              "ConserveNb=",false,
       				"MaxOcc",M});

        auto psi=makeBasisNo_e2(sites, M);
	print_state(psi);
	auto A=psi;

	for(int j=1; j<=n_e;j++)
	  {	std::vector<MPS> states;
	for(int i=1; i<=N; i+=2){
	  auto psi2=A;
   	  applyCdag(psi2, i, sites);
  applyCdag(psi2, i+1, sites);
  states.push_back(psi2);

}
	A = sum(states,{"MaxDim",Maxd,"MinDim", Mind,"Cutoff",cutoff,"ShowEigs", true});
    }

	psi=A;
	print_state(psi);
       auto ampo=makeHolstHam_FT_halffill(sites, t0, gamma, omega);

 auto H=toMPO(ampo);
 
 auto args = Args("Cutoff=",cutoff,"MaxDim",Maxd,"MinDim",Mind, "Normalize",true, "Verbose", true,"MaxDimLBO",lboMd,"MinLBO",0,"CutoffLBO",lbocutoff  ,"SVDMethod", "gesdd", "ShowEigs", false);
 auto argsMPO = Args("Cutoff=",cutoff,"MaxDim=",Maxd, "MinDim",Mind,"Normalize",false, "Verbose", false,"MaxDimLBO",lboMd,"MinLBO",1,"CutoffLBO",lbocutoff  ,"SVDMethod", "gesdd");


 AutoMPO Nel(sites);
  AutoMPO Nph(sites);
      AutoMPO Ek(sites);
    AutoMPO x(sites);
   auto curr= makeCurr_FT(sites, t0);
  

    for(int b = 1; b <=N ; b+=2)
      {

Nel += 1,"N",b ;
  x += 1,"NBdag",b ;
  x += 1,"NB",b ;
 Nph += 1,"Nph",b;


      }
        for(int b = 1; b <N-1 ; b+=2)
      {

	Ek += -t0,"Cdag",b, "C", b+2 ;
	Ek += -t0,"Cdag",b+2, "C", b ;

}
 
   for(int b = 1; b <= N; b+=2)
        {
   	  psi.position(b);

   	     auto bb = AutoMPO(sites);
   	    
  
   	     bb += 1,"NX",b;
   	 auto gg = AutoMPO(sites);
  
   		        gg += 1,"Nph",b ;
   		      auto GG = toMPO(gg);
   		      auto en2 = innerC(psi,GG,psi)/innerC(psi, psi);
   		         printfln("Nph %d %.12f",b,en2);
   			 auto gg2 = AutoMPO(sites);
  
   		        gg2 += 1,"N",b ;
   		      auto GG2 = toMPO(gg2);
   		      auto en22 = innerC(psi,GG2,psi)/innerC(psi, psi);
   		         printfln("Ne %d %.12f",b,en22);
		        

   
   }  //
     auto NEL = toMPO(Nel);
     auto NPH = toMPO(Nph);
     auto Curr = toMPO(curr);     
     auto X = toMPO(x);
     auto EK = toMPO(Ek);
     auto CDWO = toMPO(CDWOparam_FT(sites));
     auto CDWDISP = toMPO(CDWDispOparam_FT(sites));
   auto nt = int(beta/dt+(1e-9*(beta/dt)));

std::vector<double> Tsstore={10./13};
//std::vector<double> Tsstore={1./3,0.25,1./(4.5)};
//   std::vector<double> Tsstore={1.0,0.5, 0.4,0.20, 0.1};
   // std::vector<double> Tsstore={0.};
   std::vector<double> nphvec;



    std::vector<double> curvec;
   std::vector<double> curSQvec;
   std::vector<double> evec;
   std::vector<double> xvec;
   std::vector<double> ekvec;
   std::vector<double> tempvec;
    std::vector<double> lboavvec;
    std::vector<double> mxlbo_svd;
    std::vector<double> mxlbo_solv;
    std::vector<double> meanlbo_solv;
    std::vector<double> mxmd;
    std::vector<double> cdwo;
    std::vector<double> cdwdisp;


  
     int Niter=80;  
    auto sweeps = Sweeps(1);
    sweeps.maxdim() = Maxd;
    sweeps.mindim() = Mind;
    sweeps.cutoff() = cutoff;
    sweeps.niter() =Niter;

    auto nph=innerC(psi, NPH, psi)/innerC(psi, psi);
     auto ne=innerC(psi, NEL, psi)/innerC(psi, psi);
 auto ex=innerC(psi, NEL, psi)/innerC(psi, psi);
   
      printfln("\n N/L %.4f %.20f",0, nph/L);
       printfln("\n NE/L %.4f %.20f",0, ne/L);
       printfln("\n E/L %.4f %.20f",0, ex/L);
   double beta2=0;
   TDVP_lbo<MPS> C(psi,H, -dt*0.5, sweeps,args );


   std::vector<int> even;
      std::vector<int> odd;
      for(int i=1; i<=N; i+=2)
	{
	  odd.push_back(i);
	}
            for(int i=2; i<=N; i+=2)
	{
	  even.push_back(i);
	}
 int n=0;
        
    	  // Global subspace expansion

    	
    for(int n=1; n<=nt; ++n)
      {
       
	//	C.step_impl2(); 
		  C.step(); 

	beta2+=dt;
	
        auto psir2=psi;
       psir2.position(1);
       psir2.normalize();
       auto en=innerC(psir2, H, psir2);
       auto nph=innerC(psir2, NPH, psir2);
       auto ek=innerC(psir2, EK, psir2);
       auto smx=innerC(psir2, X, psir2);
       auto ne=innerC(psir2, NEL, psir2);
       auto curval=innerC(psir2, Curr, psir2);

       auto cdwo_val=innerC(psir2, CDWO, psir2);
       auto cdwdisp_val=innerC(psir2, CDWDISP, psir2);
       	  nphvec.push_back(real(nph));
       	  evec.push_back(real(en));
       	  xvec.push_back(real(smx));
       	  ekvec.push_back(real(ek));
	  curvec.push_back(real(curval));

	  cdwdisp.push_back(real(cdwdisp_val));
	  cdwo.push_back(real(cdwo_val));
     	  tempvec.push_back(1./beta2);
	  mxlbo_svd.push_back(static_cast<double>(C.get_max_lbo_svd()));
	  mxlbo_solv.push_back(static_cast<double>(C.get_max_lbo_solv()));
	  meanlbo_solv.push_back(static_cast<double>(C.get_mean_lbo_solv()));
	  mxmd.push_back(static_cast<double>(maxLinkDim(psir2)));
       std::vector<double> all_lbodims=C.get_all_lbodims_solv();
       for(size_t i=0; i<all_lbodims.size(); i++)
	 {
		    		    std::vector<double> v;
		    v.push_back(all_lbodims[i]);
		    itensor::ToFile(v,DIR+"/DATA"+"/lbodim_site"+std::to_string(i+1)+".bin", v.size());
}
	  //     	  lboavvec.push_back(C.avLboDim());

	  printfln("\n at T/ omega Energy/L %.8f %.20f",1./(beta2*omega), en/L);
             printfln("\n wt beta omega Nph/L %.8f %.20f", n*dt*omega, nph/L);
	     std::cout<< "com "<< real(en) << "  "<< real(ek)+real(smx)*gamma+omega*real(nph) <<"  lbo mx svd  "<<C.get_max_lbo_svd()<< "  lbo mx solver  "<<C.get_mean_lbo_solv()<<"  lbo mean solver "<<C.get_mean_lbo_solv()<<" max BD "<< maxLinkDim(psir2)  << "  electron number "<< ne<<std::endl;
		    
	     //	     write_local_data(psir2, sites,beta2, DIR+"/DATA");
	     //write_densdenscor(psir2, sites,beta2, DIR+"/DATA");
    
	     if(saveAll && (std::find_if(Tsstore.begin(), Tsstore.end(), dbl_cmp((1./beta2), 1E-8))!=Tsstore.end()))
		    {
	print_state(psi);

 std::string SC="aT"+std::to_string(1./beta2).substr(0, 4);
//		      Print(psir2);
		      itensor::writeToFile(DIR+"/STATES//MPS"+SC+filename,psir2);
		      itensor::writeToFile(DIR+"/STATES//siteset"+SC+filename,sites);
		      auto psi2=psi;

		     double s{0};
     double s2{0};
   for(int b = 1; b <= N; b+=1)
        {
	  psi2.position(b);
   	     auto bb = AutoMPO(sites);
  
   	     bb += 1,"N",b, "B",b ;
   	     bb += 1,"N",b, "Bdag",b ;
   		      auto BB = toMPO(bb);
   		      auto en3 = innerC(psi2,BB,psi2)/innerC(psi2, psi2);
        printfln("X %d %.12f",b,en3);
   		      auto gg = AutoMPO(sites);
  
   		        gg += 1,"Nph",b ;
   		      auto GG = toMPO(gg);
   		      auto en2 = innerC(psi2,GG,psi2)/innerC(psi2, psi2);
   		         printfln("Nph %d %.12f",b,en2);
   			 auto gg2 = AutoMPO(sites);
  
   		        gg2 += 1,"N",b ;
   		      auto GG2 = toMPO(gg2);
   		      auto en22 = innerC(psi2,GG2,psi2)/innerC(psi2, psi2);
   		         printfln("Ne %d %.12f",b,en22);
			 if((b+1)%2==0)
			   {
			 s+=real(en22);
			   }
			 else{
			   s2+=real(en22);
			 }

	}
		    }
	 
     }

  // //
    auto psi2=psi;

	 // std::cout<< " f1 "<< innerC(psi1,Ne, psi2)<<'\n';
    double s{0};
     double s2{0};
   for(int b = 1; b <= N; b+=1)
        {
	  psi2.position(b);
   	     auto bb = AutoMPO(sites);
  
   	     bb += 1,"N",b, "B",b ;
   	     bb += 1,"N",b, "Bdag",b ;
   		      auto BB = toMPO(bb);
   		      auto en3 = innerC(psi2,BB,psi2)/innerC(psi2, psi2);
        printfln("X %d %.12f",b,en3);
   		      auto gg = AutoMPO(sites);
  
   		        gg += 1,"Nph",b ;
   		      auto GG = toMPO(gg);
   		      auto en2 = innerC(psi2,GG,psi2)/innerC(psi2, psi2);
   		         printfln("Nph %d %.12f",b,en2);
   			 auto gg2 = AutoMPO(sites);
  
   		        gg2 += 1,"N",b ;
   		      auto GG2 = toMPO(gg2);
   		      auto en22 = innerC(psi2,GG2,psi2)/innerC(psi2, psi2);
   		         printfln("Ne %d %.12f",b,en22);
			 if((b+1)%2==0)
			   {
			 s+=real(en22);
			   }
			 else{
			   s2+=real(en22);
			 }
   


   }
   std::cout<< "end sum was in phys "<<s<<std::endl;
   std::cout<< "end sum was in aux "<<s2<<std::endl;

  		 itensor::bin_write(DIR+"/DATA"+"/temp.bin", tempvec);
  		 itensor::bin_write(DIR+"/DATA"+"/nX.bin", xvec);
  		 itensor::bin_write(DIR+"/DATA"+"/CDWDISP.bin", cdwdisp);
  		 itensor::bin_write(DIR+"/DATA"+"/CDWO.bin", cdwo);
		 itensor::bin_write(DIR+"/DATA"+"/J.bin", curvec);
		 //		 itensor::bin_write(DIR+"/DATA"+"/JJ.bin", curSQvec);
  		 itensor::bin_write(DIR+"/DATA"+"/EK.bin", ekvec);
  		 itensor::bin_write(DIR+"/DATA"+"/Nph.bin", nphvec);
  		 itensor::bin_write(DIR+"/DATA"+"/E.bin", evec);
		 itensor::bin_write(DIR+"/DATA"+"/mxLBO_svd.bin", mxlbo_svd);
		 itensor::bin_write(DIR+"/DATA"+"/mxLBO_solv.bin", mxlbo_solv);
		 itensor::bin_write(DIR+"/DATA"+"/meanLBO_solv.bin", meanlbo_solv);
		 itensor::bin_write(DIR+"/DATA"+"/mxBD.bin", mxmd);
		
		 
    return 0;
    
    }

