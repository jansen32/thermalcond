#include<iostream>
#define EIGEN_USE_MKL_ALL
#include"basis.hpp"
#include"operators.hpp"
#include"diag.h"
#include"tpoperators.hpp"
#include"thermal_edops.hpp"
#include"files.hpp"
#include"timeev.hpp"
#include"ETH.hpp"
#include<iomanip>
#include <boost/program_options.hpp>

using namespace boost::program_options;
int main(int argc, char *argv[])
{
  using namespace Eigen;
using namespace std;
using namespace Many_Body;
  using Fermi_HubbardBasis= TensorProduct<ElectronBasis, ElectronBasis>;
   using HolsteinBasis= TensorProduct<ElectronBasis, PhononBasis>;
  using Mat= Operators::Mat;
   using boost::program_options::value;
   int M{};
  int L{};
  int Ne{};

  double t0{};
  double omega{};
  double gamma{};
  double cutoff{};
  bool PB{};


  std::string sM{};
  std::string sL{};
  std::string star{};

  std::string st0{};
  std::string somega{};
  std::string sgamma{};
  std::string starget{};
  std::string scutoff{};
  std::string sPB{};
  std::string DIRNAME{};

  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("L", value(&L)->default_value(4), "L")
      ("M,m", value(&M)->default_value(2), "M")
      ("Ne", value(&Ne)->default_value(1), "ne")
      ("t0", value(&t0)->default_value(1.), "t0")
      ("gam", value(&gamma)->default_value(std::sqrt(2)), "gamma")
      ("omg", value(&omega)->default_value(1.), "omega")
      ("d", value(&DIRNAME)->default_value("noname"), "d")
      ("pb", value(&PB)->default_value(0), "pb");
  boost::program_options::variables_map vm;
  boost::program_options::store(parse_command_line(argc, argv, desc), vm);
  boost::program_options::notify(vm);

  if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("L"))
      {      std::cout << "L: " << vm["L"].as<int>() << '\n';

      }
         if (vm.count("M"))
      {      std::cout << "M: " << vm["M"].as<int>() << '\n';

      }
         if (vm.count("Ne"))
      {      std::cout << "Ne: " << vm["Ne"].as<int>() << '\n';


      }
      	 if (vm.count("t0"))
      {      std::cout << "t0: " << vm["t0"].as<double>() << '\n';

      }
      	 	 if (vm.count("omg"))
      {      std::cout << "omega: " << vm["omg"].as<double>() << '\n';

      }
		 
      		 if (vm.count("gam"))
      {      std::cout << "gamma: " << vm["gam"].as<double>() << '\n';

      }

		 if (vm.count("pb"))
      {      std::cout << "PB: " << vm["pb"].as<bool>() << '\n';

      }
      }
    }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }

  double mean=0.5*L*M*omega;
  double dt=0.1;

  ElectronBasis e( L, Ne);
  //    std::cout<< e<<std::endl;
  
  PhononBasis ph(L, M);

      HolsteinBasis TP(e, ph);
      std::cout<< TP.dim << std::endl;             
         Eigen::VectorXcd inistate(TP.dim);

	 Eigen::MatrixXd Ham=Eigen::MatrixXd::Zero(TP.dim, TP.dim);
	 Eigen::MatrixXd A=Eigen::MatrixXd::Zero(TP.dim, TP.dim);
	 Eigen::MatrixXd A_ne=Eigen::MatrixXd::Zero(TP.dim, TP.dim);
    Mat Cur_E(TP.dim, TP.dim);
    double C=1;
    for(int i=0; i<L-1; i++)
      {

	Cur_E+=Operators::CurCoupOperator_loc(TP, e,ph,i,i+1, -t0*gamma);
	A+=(i)*(Operators::EKinLocalOperator(TP, e,i,i+1, t0)+Operators::NumberOperatorph_1(TP, ph,omega, i,PB));
	A+=(i)*(Eigen::MatrixXd(Operators::Elecnr_localOperator(TP, e,i,gamma))*(Eigen::MatrixXd(Operators::BosonDOperator_1(TP, ph,C,i,PB))+Eigen::MatrixXd(Operators::BosonCOperator_1(TP, ph,C,i,PB))));
	A_ne+=(i)*(Eigen::MatrixXd(Operators::Elecnr_localOperator(TP, e,i,1)));

    Ham+=Operators::EKinLocalOperator(TP, e,i,i+1, t0)+Operators::NumberOperatorph_1(TP, ph,omega, i,PB);
    Ham+=Eigen::MatrixXd(Operators::Elecnr_localOperator(TP, e,i,gamma))*(Eigen::MatrixXd(Operators::BosonDOperator_1(TP, ph,C,i,PB))+Eigen::MatrixXd(Operators::BosonCOperator_1(TP, ph,C,i,PB)));

}
    A+=(L-1)*(Operators::NumberOperatorph_1(TP, ph,omega, L-1,PB));
 A+=(L-1)*(Eigen::MatrixXd(Operators::Elecnr_localOperator(TP, e,L-1,gamma))*(Eigen::MatrixXd(Operators::BosonDOperator_1(TP, ph,C,L-1,PB))+Eigen::MatrixXd(Operators::BosonCOperator_1(TP, ph,C,L-1,PB))));
 A_ne+=(L-1)*(Eigen::MatrixXd(Operators::Elecnr_localOperator(TP, e,L-1,1)));

  Ham+=Operators::NumberOperatorph_1(TP, ph,omega, L-1,PB);
  Ham+=Eigen::MatrixXd(Operators::Elecnr_localOperator(TP, e,L-1,gamma))*(Eigen::MatrixXd(Operators::BosonDOperator_1(TP, ph,C,L-1,PB))+Eigen::MatrixXd(Operators::BosonCOperator_1(TP, ph,C,L-1,PB)));



for(int i=1; i<L-1; i++)
      {
Cur_E+=Operators::CurrLocalOperator(TP, e,i-1,i+1, t0*t0);
      }
      Mat E1=Operators::EKinOperatorL(TP, e, t0,PB);
       Mat Ebdag=Operators::NBosonCOperator(TP, ph, gamma, PB);
       Mat Eb=Operators::NBosonDOperator(TP, ph, gamma, PB);
      Mat Eph=Operators::NumberOperator(TP, ph, omega,  PB);

      Eigen::VectorXd eigenVals(TP.dim);
      Mat H_2=E1+Eb+Eph+Ebdag;

    Eigen::MatrixXd HH=Eigen::MatrixXd(H_2);

    	  Many_Body::diagMat(HH, eigenVals);
	  std::cout<< eigenVals<<std::endl;
    		      bin_write(DIRNAME+"/E.bin", eigenVals);
}
 
