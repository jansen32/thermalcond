#include<iostream>
#define EIGEN_USE_MKL_ALL
#include"basis.hpp"
#include"operators.hpp"
#include"diag.h"
#include"thermal_edops.hpp"
#include"files.hpp"
#include"timeev.hpp"
#include"ETH.hpp"
#include"tpoperators.hpp"
#include <boost/program_options.hpp>
int main(int argc, char *argv[])
{
  using namespace Eigen;
using namespace std;
using namespace Many_Body;
  using Fermi_HubbardBasis= TensorProduct<ElectronBasis, ElectronBasis>;
   using HolsteinBasis= TensorProduct<ElectronBasis, PhononBasis>;
  using Mat= Operators::Mat;
  using boost::program_options::value;
   size_t M{};
  size_t L{};
  int Ne{};
  double t0{};
  double omega{};
  double omegap{};
  double gamma{};
  double T{};
  double dt{};
  double tot{};
  bool PB{};
  std::string dirname="";
  std::string filename={};
  
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("L", value(&L)->default_value(4), "L")
      ("M,m", value(&M)->default_value(2), "M")
      ("Ne", value(&Ne)->default_value(1), "ne")
      ("T", value(&T)->default_value(1.), "T")
      ("t0", value(&t0)->default_value(1.), "t0")
      ("gam", value(&gamma)->default_value(1.), "gamma")
      ("omg", value(&omega)->default_value(1.), "omega")
      ("omgp", value(&omegap)->default_value(1.), "omegap")
      ("dir", value(&dirname)->default_value(""), "dirname")
    ("dt", value(&dt)->default_value(0.1), "dt")
      ("tot", value(&tot)->default_value(1.), "tot")
    ("pb", value(&PB)->default_value(true), "PB");
  


    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);

  if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("L"))
      {      std::cout << "L: " << vm["L"].as<size_t>() << '\n';
	filename+="L"+std::to_string(vm["L"].as<size_t>());
      }
     if (vm.count("M"))
      {
	std::cout << "M: " << vm["M"].as<size_t>() << '\n';
	filename+="M"+std::to_string(vm["M"].as<size_t>());
      }
         if (vm.count("Ne"))
      {      std::cout << "Ne: " << vm["Ne"].as<int>() << '\n';


      }
      if (vm.count("T"))
      {
	std::cout << "T: " << vm["T"].as<double>() << '\n';
	filename+="T"+std::to_string(vm["T"].as<double>()).substr(0, 4);
      }
      if (vm.count("t0"))
      {
	std::cout << "t0: " << vm["t0"].as<double>() << '\n';
	filename+="t0"+std::to_string(vm["t0"].as<double>()).substr(0, 6);
      }
       if (vm.count("omg"))
      {
	std::cout << "omega: " << vm["omg"].as<double>() << '\n';
	filename+="omega"+std::to_string(vm["omg"].as<double>()).substr(0, 6);
      }

       if (vm.count("gam"))
      {
	std::cout << "gamma: " << vm["gam"].as<double>() << '\n';
	filename+="gam"+std::to_string(vm["gam"].as<double>()).substr(0, 6);
      }
       if (vm.count("dt"))
      {
	std::cout << "dt: " << vm["dt"].as<double>() << '\n';
	filename+="dt"+std::to_string(vm["dt"].as<double>()).substr(0, 6);
      }
       if (vm.count("tot"))
      {
	std::cout << "total time: " << vm["tot"].as<double>() << '\n';
	filename+="tot"+std::to_string(vm["tot"].as<double>()).substr(0, 6);
      }

       if (vm.count("pb"))
      {
	std::cout << "PB: " << vm["pb"].as<bool>() << '\n';
	filename+="PB"+std::to_string(vm["pb"].as<bool>());
      }
    }
  }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }




  ElectronBasis e( L, Ne);

  PhononBasis ph(L, M);
  //         std::cout<< ph<<std::endl;
      HolsteinBasis TP(e, ph);
 
 		filename+=".bin";
	   


 		std::cout<< "dim "<< TP.dim << std::endl;  


 		Mat E1=Operators::EKinOperatorL(TP, e, t0, PB);

 		Mat Ebdag=Operators::NBosonCOperator(TP, ph, gamma, PB);
 		Mat Eb=Operators::NBosonDOperator(TP, ph, gamma, PB);
 		Mat Eph=Operators::NumberOperator(TP, ph, omega,  PB);

		//-gam sun x_i /2
		double C=1;
		Mat Eextra=-gamma*0.5*((Operators::BosonDOperator_1(TP, ph,C,0,PB))+(Operators::BosonCOperator_1(TP, ph,C,0,PB)));
		for(int i=1; i<L;i++)
		  {
		    Eextra+=-gamma*0.5*((Operators::BosonDOperator_1(TP, ph,C,i,PB))+(Operators::BosonCOperator_1(TP, ph,C,i,PB)));
		  }
	 Eigen::MatrixXd A=Eigen::MatrixXd::Zero(TP.dim, TP.dim);      

    Mat Cur_E(TP.dim, TP.dim);

    for(int i=0; i<L-1; i++)
      {

	Cur_E+=Operators::CurCoupOperator_loc(TP, e,ph,i,i+1, -t0*gamma);
	A+=(i)*(Operators::EKinLocalOperator(TP, e,i,i+1, t0)+Operators::NumberOperatorph_1(TP, ph,omega, i,PB));
	A+=(i)*(Eigen::MatrixXd(Operators::Elecnr_localOperator(TP, e,i,gamma))*(Eigen::MatrixXd(Operators::BosonDOperator_1(TP, ph,C,i,PB))+Eigen::MatrixXd(Operators::BosonCOperator_1(TP, ph,C,i,PB))));
      }
    A+=(L-1)*(Operators::NumberOperatorph_1(TP, ph,omega, L-1,PB));
 A+=(L-1)*(Eigen::MatrixXd(Operators::Elecnr_localOperator(TP, e,L-1,gamma))*(Eigen::MatrixXd(Operators::BosonDOperator_1(TP, ph,C,L-1,PB))+Eigen::MatrixXd(Operators::BosonCOperator_1(TP, ph,C,L-1,PB))));
for(int i=1; i<L-1; i++)
      {
Cur_E+=Operators::CurrLocalOperator(TP, e,i-1,i+1, t0*t0);
      }
       Mat H=E1+Eph  +Ebdag + Eb+Eextra;
            Eigen::MatrixXcd Cur_Ecp=(Cur_E);
	    Cur_Ecp*=-std::complex<double>(0,1);  	   
            Eigen::MatrixXcd COMM_Je=std::complex<double>(0,1)*(A*Cur_Ecp-Cur_Ecp*A);
   auto J=Operators::CurOperatorL(TP, e, t0, PB);    

 
       Eigen::VectorXd eigenVals(TP.dim);
        Eigen::VectorXd eigenVals2(TP.dim);
    
       Eigen::MatrixXd HH=Eigen::MatrixXd(H);
          Eigen::MatrixXd JJ=Eigen::MatrixXd(J);

 	  Eigen::MatrixXcd JJcp=JJ*std::complex<double>(0,1);
	  //std::cout<< HH<<std::endl;
              Many_Body::diagMat(HH, eigenVals);
	   Eigen::MatrixXd EKIN=HH.adjoint()*E1*HH;
	      std::cout<< "EV  "<<std::endl<<eigenVals(0)<<std::endl;
	      Eigen::MatrixXcd HH_cp=HH.cast<std::complex<double>>();

 
	      Eigen::SparseMatrix<std::complex<double>, Eigen::RowMajor> rho(TP.dim, TP.dim);
    double beta=1./T;
     std::vector<double> ek(TP.dim);
     std::vector<double> comm_je(TP.dim);
    	   std::vector<double> ensvec(TP.dim);
	    Eigen::VectorXd ek_vec=EKIN.diagonal();
	    Eigen::VectorXd comm_je_vec=COMM_Je.diagonal().real();
    double Z=0;
    for(int i=0; i<eigenVals.size(); i++)
	{
	double exponent=-(eigenVals[i]-eigenVals[0]);
	exponent*=beta;
	double ex=std::exp(exponent);
	rho.coeffRef(i, i)=ex;
	Z+=ex;
	      ek[i]=ek_vec(i);
	      comm_je[i]=comm_je_vec(i);
      ensvec[i]=eigenVals(i);
	}

    rho/=Z;
  int i=0;
  
   Eigen::MatrixXcd JJmat=HH_cp.adjoint()*(JJcp.selfadjointView<Eigen::Lower>()*HH_cp);
   Eigen::MatrixXcd JeJemat=HH_cp.adjoint()*(Cur_Ecp.selfadjointView<Eigen::Lower>()*HH_cp);
   std::cout<< JJmat.cols()<< " and "<< JJmat.rows()<<std::endl;



	     std::cout<< "EK = "<<expvalCan(ensvec, ek, T)<<std::endl;
	     	     std::cout<< "Je commutator = "<<expvalCan(ensvec, comm_je, T)<<std::endl;
         while(i*dt<tot)
        {

     Eigen::SparseMatrix<std::complex<double>, Eigen::RowMajor> evExp=TimeEv::EigenvalExponent(eigenVals, i*dt);
      Eigen::SparseMatrix<std::complex<double>, Eigen::RowMajor> evExp_MIN=TimeEv::EigenvalExponent(eigenVals, -i*dt);
	  auto M1_J=evExp*JJmat;
	  auto M2_J=rho*evExp_MIN*JJmat;


	  auto M1_Je=evExp*JeJemat;
	  auto M2_Je=rho*evExp_MIN*JeJemat;
      std::complex<double> val_J=(M2_J*M1_J).trace();
      std::complex<double> val_Je=(M2_Je*M1_Je).trace();


  std::vector<std::complex<double>> JJ_vec;
  std::vector<std::complex<double>> JeJe_vec;
   std::vector<double> t_vec;
  JJ_vec.push_back(val_J);
  JeJe_vec.push_back(val_Je);
   t_vec.push_back(i*dt);
   size_t vecSize=t_vec.size();
       ToFile(JJ_vec, dirname+"/JJ.dat",vecSize);      
   ToFile(JeJe_vec, dirname+"/JeJe.dat",vecSize);      
       ToFile(t_vec, dirname+"/time.dat",vecSize);      


    	 i++;
  	 std::cout<< i*dt<<"  at J "<<val_J << " Je "<<val_Je << " "<<std::endl;
        } 	   


  
  

  return 0;
}
 
