#include<iostream>
#define EIGEN_USE_MKL_ALL
#include"basis.hpp"
#include"operators.hpp"
#include"diag.h"
#include"tpoperators.hpp"
#include"thermal_edops.hpp"
#include"files.hpp"
#include"timeev.hpp"
#include"ETH.hpp"
#include<iomanip>
#include <boost/program_options.hpp>

using namespace boost::program_options;
int main(int argc, char *argv[])
{
  using namespace Eigen;
using namespace std;
using namespace Many_Body;
  using Fermi_HubbardBasis= TensorProduct<ElectronBasis, ElectronBasis>;
   using HolsteinBasis= TensorProduct<ElectronBasis, PhononBasis>;
  using Mat= Operators::Mat;
   using boost::program_options::value;
   int M{};
  int L{};
  int Ne{};

  double t0{};
  double omega{};
  double gamma{};
  double cutoff{};
  bool PB{};


  std::string sM{};
  std::string sL{};
  std::string star{};

  std::string st0{};
  std::string somega{};
  std::string sgamma{};
  std::string starget{};
  std::string scutoff{};
  std::string sPB{};
  std::string DIRNAME{};

  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("L", value(&L)->default_value(4), "L")
      ("M,m", value(&M)->default_value(2), "M")
      ("Ne", value(&Ne)->default_value(1), "ne")
      ("t0", value(&t0)->default_value(1.), "t0")
      ("gam", value(&gamma)->default_value(std::sqrt(2)), "gamma")
      ("omg", value(&omega)->default_value(1.), "omega")
      ("d", value(&DIRNAME)->default_value("noname"), "d")
      ("pb", value(&PB)->default_value(0), "pb");
  boost::program_options::variables_map vm;
  boost::program_options::store(parse_command_line(argc, argv, desc), vm);
  boost::program_options::notify(vm);

  if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("L"))
      {      std::cout << "L: " << vm["L"].as<int>() << '\n';

      }
         if (vm.count("M"))
      {      std::cout << "M: " << vm["M"].as<int>() << '\n';

      }
         if (vm.count("Ne"))
      {      std::cout << "Ne: " << vm["Ne"].as<int>() << '\n';


      }
      	 if (vm.count("t0"))
      {      std::cout << "t0: " << vm["t0"].as<double>() << '\n';

      }
      	 	 if (vm.count("omg"))
      {      std::cout << "omega: " << vm["omg"].as<double>() << '\n';

      }
		 
      		 if (vm.count("gam"))
      {      std::cout << "gamma: " << vm["gam"].as<double>() << '\n';

      }

		 if (vm.count("pb"))
      {      std::cout << "PB: " << vm["pb"].as<bool>() << '\n';

      }
      }
    }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }

  double mean=0.5*L*M*omega;
  double dt=0.1;

  ElectronBasis e( L, Ne);
  //    std::cout<< e<<std::endl;
  
  PhononBasis ph(L, M);

      HolsteinBasis TP(e, ph);
      std::cout<< TP.dim << std::endl;             
         Eigen::VectorXcd inistate(TP.dim);

	 Eigen::MatrixXd Ham=Eigen::MatrixXd::Zero(TP.dim, TP.dim);
	 Eigen::MatrixXd A=Eigen::MatrixXd::Zero(TP.dim, TP.dim);
	 Eigen::MatrixXd A_ne=Eigen::MatrixXd::Zero(TP.dim, TP.dim);
    Mat Cur_E(TP.dim, TP.dim);
    double C=1;
    for(int i=0; i<L-1; i++)
      {

	Cur_E+=Operators::CurCoupOperator_loc(TP, e,ph,i,i+1, -t0*gamma);
	A+=(i)*(Operators::EKinLocalOperator(TP, e,i,i+1, t0)+Operators::NumberOperatorph_1(TP, ph,omega, i,PB));
	A+=(i)*(Eigen::MatrixXd(Operators::Elecnr_localOperator(TP, e,i,gamma))*(Eigen::MatrixXd(Operators::BosonDOperator_1(TP, ph,C,i,PB))+Eigen::MatrixXd(Operators::BosonCOperator_1(TP, ph,C,i,PB))));
	A_ne+=(i)*(Eigen::MatrixXd(Operators::Elecnr_localOperator(TP, e,i,1)));

    Ham+=Operators::EKinLocalOperator(TP, e,i,i+1, t0)+Operators::NumberOperatorph_1(TP, ph,omega, i,PB);
    Ham+=Eigen::MatrixXd(Operators::Elecnr_localOperator(TP, e,i,gamma))*(Eigen::MatrixXd(Operators::BosonDOperator_1(TP, ph,C,i,PB))+Eigen::MatrixXd(Operators::BosonCOperator_1(TP, ph,C,i,PB)));

}
    A+=(L-1)*(Operators::NumberOperatorph_1(TP, ph,omega, L-1,PB));
 A+=(L-1)*(Eigen::MatrixXd(Operators::Elecnr_localOperator(TP, e,L-1,gamma))*(Eigen::MatrixXd(Operators::BosonDOperator_1(TP, ph,C,L-1,PB))+Eigen::MatrixXd(Operators::BosonCOperator_1(TP, ph,C,L-1,PB))));
 A_ne+=(L-1)*(Eigen::MatrixXd(Operators::Elecnr_localOperator(TP, e,L-1,1)));

  Ham+=Operators::NumberOperatorph_1(TP, ph,omega, L-1,PB);
  Ham+=Eigen::MatrixXd(Operators::Elecnr_localOperator(TP, e,L-1,gamma))*(Eigen::MatrixXd(Operators::BosonDOperator_1(TP, ph,C,L-1,PB))+Eigen::MatrixXd(Operators::BosonCOperator_1(TP, ph,C,L-1,PB)));



for(int i=1; i<L-1; i++)
      {
Cur_E+=Operators::CurrLocalOperator(TP, e,i-1,i+1, t0*t0);
      }
      Mat E1=Operators::EKinOperatorL(TP, e, t0,PB);
       Mat Ebdag=Operators::NBosonCOperator(TP, ph, gamma, PB);
       Mat Eb=Operators::NBosonDOperator(TP, ph, gamma, PB);
      Mat Eph=Operators::NumberOperator(TP, ph, omega,  PB);

      Eigen::VectorXd eigenVals(TP.dim);
      Mat H_2=E1+Eb+Eph+Ebdag;

       Eigen::MatrixXcd comm=(A*Ham-Ham*A);
       //	    	    comm*=std::complex<double>(0,1);

            Eigen::MatrixXcd Cur_Ecp=(Cur_E);
	    Cur_Ecp*=-std::complex<double>(0,1);
            Eigen::MatrixXcd COMM_Je=std::complex<double>(0,1)*(A*Cur_Ecp-Cur_Ecp*A);
	    //	    			  std::cout<< Eigen::MatrixXcd(Cur_Ecp)<<std::endl;
				  //			  std::cout<<std::endl;
				  //	  std::cout<< Eigen::MatrixXd(Ham)<<std::endl;
	    //		  std::cout<<std::endl;
	    //		         			  std::cout<< Eigen::MatrixXcd(Cur_Ecp)<<std::endl;
	    //			  std::cout<<std::endl;

				 std::cout<<" Hams is approx "<<std::endl;
				 //			   std::cout<< (Eigen::MatrixXd(Ham).isApprox(Eigen::MatrixXd(H_2)))<<std::endl;
				 //	  			 std::cout<< (Eigen::MatrixXd(Ham)-Eigen::MatrixXd(H_2)).minCoeff()<<std::endl;

				  std::cout<<" Comm is approx "<<std::endl;
				   std::cout<< comm.isApprox(Cur_Ecp)<<std::endl;
				   //			  			  				  std::cout<<comm-Cur_Ecp<<std::endl;
											  //	  			  std::cout<< (Eigen::MatrixXcd(comm-Cur_Ecp).abs()).minCoeff()<<std::endl;

	  //std::cout<<std::endl;
	  //std::cout<< Eigen::MatrixXd(O_2)<<std::endl;



//    	 Mat O=Operators::NumberOperator(TP, ph, 1,  PB);
//    inistate.setZero();

//    inistate[0]=1;
//    	          Eigen::VectorXcd i0=inistate;
      
//    		  std::cout<< TP.dim << std::endl;     

//       // make and check holstein currcoup exacct
//    auto J=Operators::CurOperatorL(TP, e, t0, PB);    
// Eigen::MatrixXcd Jcp=J*std::complex<double>(0,1);      

//     Eigen::MatrixXd HH=Eigen::MatrixXd(Ham);

//     //Eigen::MatrixXcd HHcp=HH;
//     Eigen::MatrixXcd HHcp=Eigen::MatrixXd(HH);
//              Eigen::MatrixXcd M2=Jcp*A_ne;
//              Eigen::MatrixXcd M1=A_ne*Jcp;
//     	     Eigen::MatrixXcd COMM_J=std::complex<double>(0,1)*(M1-M2);
//     	     //                  std::cout<< M1<<std::endl;
//     	     //           std::cout<< COMM<<std::endl;
//     	     //   std::cout<< M2<<std::endl;

		  
//     		  //           std::cout<< HHcp<<std::endl;
//     		  //           std::cout<< Jcp<<std::endl;
//         Eigen::MatrixXd N=Eigen::MatrixXd(Eph);
//     	  Many_Body::diagMat(HH, eigenVals);
//      std::cout<<"MIN E "<<std::setprecision(15)<< eigenVals(0)<< std::endl;
//      std::cout<< "MAX "<<eigenVals(eigenVals.size()-1)<< std::endl;
//     		      std::cout<<endl<<eigenVals.mean()-mean<<std::endl;
//          Eigen::VectorXd energy(TP.dim);
//     	   std::vector<double> nph(TP.dim);
//     	   std::vector<double> ek(TP.dim);
//     	   std::vector<double> ec(TP.dim);
//     	   std::vector<double> j(TP.dim);
//     	   std::vector<double> jj(TP.dim);
//     	   std::vector<double> je(TP.dim);
//     	   std::vector<double> jeje(TP.dim);
//     	   std::vector<double> comm_je(TP.dim);
//     	   std::vector<double> comm_j(TP.dim);
//     	   //std::vector<double> obs(TP.dim);
//     	   std::vector<double> ensvec(TP.dim);
	     
//     	   Eigen::MatrixXd PHD2=HH.adjoint()*N*HH;
//     	   Eigen::MatrixXd EKIN=HH.adjoint()*E1*HH;
//     	   Eigen::MatrixXd EC=HH.adjoint()*(Ebdag+Eb)*HH;
//     	   Eigen::MatrixXcd Jmat=HH.adjoint()*(Jcp*HH);
//     	   Eigen::MatrixXcd JEmat=HH.adjoint()*(Cur_Ecp*HH);
//     	   Eigen::MatrixXcd JJmat=Jmat*Jmat;
//     	   Eigen::MatrixXcd COMMJmat=HH.adjoint()*(COMM_J*HH);
//     	   Eigen::MatrixXcd COMMJEmat=HH.adjoint()*(COMM_Je*HH);
//     	   Eigen::MatrixXcd JEJEmat=JEmat*JEmat;
//     	   //	   std::complex<double> J_in=(newIn.adjoint()*(Jcp*newIn))(0);
//     	   // std::complex<double> JJsq_in=(newIn.adjoint()*(Jcp*Jcp*newIn))(0);
//     	    Eigen::VectorXd nph_vec=PHD2.diagonal();
//     	    Eigen::VectorXd ek_vec=EKIN.diagonal();
//     	    Eigen::VectorXd ec_vec=EC.diagonal();
//     	    Eigen::VectorXd jj_vec=(JJmat.diagonal().real());
//     	     Eigen::VectorXd j_vec=(Jmat.diagonal().real());
//     	     Eigen::VectorXd comm_j_vec=(COMMJmat.diagonal().real());
//     	     Eigen::VectorXd comm_je_vec=(COMMJEmat.diagonal().real());
//     	     Eigen::VectorXd jeje_vec=(JEJEmat.diagonal().real());
//     	     Eigen::VectorXd je_vec=(JEmat.diagonal().real());
//     	     std::cout<< jj_vec<<std::endl;
//     	    std::cout<< "GS "<<eigenVals(0)<<std::endl;
//     	   for(int i=0; i<energy.size(); i++)
//     {
//       //obs[i]=real(obstot[i]);
      
//       ensvec[i]=eigenVals(i);
//       nph[i]=nph_vec(i);
//       ek[i]=ek_vec(i);
//       ec[i]=ec_vec(i);
//       j[i]=j_vec(i);
//       jj[i]=jj_vec(i);
//       je[i]=je_vec(i);
//       jeje[i]=jeje_vec(i);
//       comm_je[i]=comm_je_vec(i);
//       comm_j[i]=comm_j_vec(i);
//       //      std::cout<< COMMmat(i,i)<<std::endl;
//       if(gamma!=0)
//     	{	ec[i]/=gamma;}

//       //std::cout<< ensvec[i]<<'\n';
//     }// 	   	    std::cout<< "gS "<<   eigenVals[0] <<std::endl;
//     		    std::cout<< "mean "<<   eigenVals.mean() <<std::endl;
//     	   std::vector<double> nph_data;
//     	    std::vector<double> ek_data;
//     	     std::vector<double> ec_data;
//     	     std::vector<double> j_data;
//     	     	     std::vector<double> jj_data;
//     	     	     std::vector<double> je_data;
//     	     	     std::vector<double> jeje_data;
//     		     std::vector<double> comm_j_data;
//     		     std::vector<double> comm_je_data;

//     	    	   std::vector<double> E_data;
//     	    	   //		   std::vector<double> Tr={0.01, 0.05, 0.1, 0.15, 0.2, 0.25};
//     	    	   		   std::vector<double> Tr;
//     	    	   for(int i=1; i<200; i++)
//     	    	     {
//     	    	       Tr.push_back(i*0.05);
		       
//     	    	     }

		   
//     	    	  	  for(auto t: Tr){
	     
//     	    	 // //	      std::cout<< l<<std::endl;
//     	         // bin_write(filename, l);
//     	    	 // n++;
//     			    double val_e=expvalCan(ensvec, ensvec,  t);
//     			    double val_nph=expvalCan(ensvec, nph,  t);
//     			    double val_ec=expvalCan(ensvec, ec,  t);
//     			    double val_ek=expvalCan(ensvec, ek,  t);
//     			    double val_j=expvalCan(ensvec, j,  t);
//     			     double val_jj=expvalCan(ensvec, jj,  t);
//     			     double val_je=expvalCan(ensvec, je,  t);
//     			     double val_jeje=expvalCan(ensvec, jeje,  t);
//     			     double val_comm_je=expvalCan(ensvec, comm_je,  t);
//     			     double val_comm_j=expvalCan(ensvec, comm_j,  t);
//     	    		    std::cout<< " at T "<< t<<std::endl;
//     			    std::cout<<std::setprecision(15)<<val_e <<"  " <<" Nph "<<val_jj << std::endl;
//     			    nph_data.push_back(val_nph);
//     			    ek_data.push_back(val_ek);
//     			    ec_data.push_back(val_ec);
//     			    j_data.push_back(val_j);
//     			    jj_data.push_back(val_jj);
//     			    je_data.push_back(val_je);
//     			    jeje_data.push_back(val_jeje);
//     			    comm_je_data.push_back(val_comm_je);
//     			    comm_j_data.push_back(val_comm_j);
//     			    E_data.push_back(val_e);

//     	    }
 
//     			  int pb=PB;
//     		  	  std::string Hs="H";
//     			  std::string Ts="T";
//     			  std::string phds="PHD";
	  
//     		      bin_write(DIRNAME+"/E.bin", E_data);
//     		      bin_write(DIRNAME+"/Nph.bin", nph_data);
//     		      bin_write(DIRNAME+"/EK.bin", ek_data);
//     		      bin_write(DIRNAME+"/nX.bin", ec_data);
//     		      bin_write(DIRNAME+"/JJ.bin", jj_data);
//     		      bin_write(DIRNAME+"/J.bin", j_data);
//     		      bin_write(DIRNAME+"/JeJe.bin", jeje_data);
//     		      bin_write(DIRNAME+"/AeJe_comm.bin", comm_je_data);
//     		      bin_write(DIRNAME+"/AnJ_comm.bin", comm_j_data);
//     		      bin_write(DIRNAME+"/Je.bin", je_data);
//     		      bin_write(DIRNAME+"/temp.bin", Tr);
  return 0;
}
 
