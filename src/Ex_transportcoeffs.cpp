#include<iostream>
#define EIGEN_USE_MKL_ALL
#include"basis.hpp"
#include"operators.hpp"
#include"diag.h"
#include"thermal_edops.hpp"
#include"files.hpp"
#include"timeev.hpp"
#include"ETH.hpp"
#include"tpoperators.hpp"
#include <boost/program_options.hpp>

double Lor(double freq, double eta, double dE)
{
  return (eta)/((freq-dE)*(freq-dE)+eta*eta);
}
int main(int argc, char *argv[])
{
  using namespace Eigen;
using namespace std;
using namespace Many_Body;
  using Fermi_HubbardBasis= TensorProduct<ElectronBasis, ElectronBasis>;
   using HolsteinBasis= TensorProduct<ElectronBasis, PhononBasis>;
  using Mat= Operators::Mat;
  using boost::program_options::value;
   size_t M{};
  size_t L{};
  int Ne{};
  double eta{};
  double dom{};
  double om_max{};
  double T{};
  std::string params;
  bool PB{};
  std::string dirname="";
  
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("L", value(&L)->default_value(4), "L")
      ("M,m", value(&M)->default_value(2), "M")
      ("Ne", value(&Ne)->default_value(1), "ne")
      ("eta", value(&eta)->default_value(1), "eta")
      ("dom", value(&dom)->default_value(1), "dom")
      ("T", value(&T)->default_value(1), "T")
      ("om_max", value(&om_max)->default_value(1.), "om_max")
      ("dir", value(&dirname)->default_value(""), "dirname")
      ("params", value(&params)->default_value(""), "params");
  


    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);

  if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("L"))
      {      std::cout << "L: " << vm["L"].as<size_t>() << '\n';

      }
     if (vm.count("M"))
      {
	std::cout << "M: " << vm["M"].as<size_t>() << '\n';

      }
         if (vm.count("Ne"))
      {      std::cout << "Ne: " << vm["Ne"].as<int>() << '\n';

      }
       
    }
  }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }




  ElectronBasis e( L, Ne);

  PhononBasis ph(L, M);

      HolsteinBasis TP(e, ph);
 
	 Eigen::MatrixXcd comm_J=Eigen::MatrixXcd::Zero(TP.dim, TP.dim);    
	 Eigen::MatrixXcd comm_Je=Eigen::MatrixXcd::Zero(TP.dim, TP.dim);        
    
	 Eigen::MatrixXcd JJ=Eigen::MatrixXcd::Zero(TP.dim, TP.dim);        
	 Eigen::MatrixXcd JeJe=Eigen::MatrixXcd::Zero(TP.dim, TP.dim);        
	  Eigen::VectorXd E=Eigen::VectorXd::Zero(TP.dim);        
	  Eigen::VectorXd EK=Eigen::VectorXd::Zero(TP.dim);        

       	   bin_read(dirname+"/comm_Jmat.bin", comm_J);
       	   bin_read(dirname+"/comm_Jemat.bin", comm_Je);
       	   bin_read(dirname+"/JJmat.bin", JJ);
       	   bin_read(dirname+"/JeJemat.bin", JeJe);
   	     bin_read(dirname+"/E.bin", E);
   	     bin_read(dirname+"/EKdiag.bin", EK);
	     
	     std::vector<double> omega;
	     
	     //	     std::cout<< JJ<<std::endl;
	     //std::cout<< JeJe<<std::endl;
	     for(double d=-om_max; d<=om_max;d+=dom )
	       {
		 
  omega.push_back(d);
}
	     std::vector<double> dataJJ(omega.size(), 0.0);
   std::vector<double> dataJeJe(omega.size(), 0.0);
	     
	     std::cout<<std::endl;
	     
	     double Z=0;
	     
	     for(int j=0; j<JJ.rows(); j++) //
	       {
		 std::cout <<j << "from " << JJ.rows()<<std::endl; 
		 Z+=std::exp(-E(j)/T);
for(int i=0; i<JJ.rows(); i++) //
	       {
		 double diff=E(i)-E(j);
		 for(int l=0; l<omega.size(); l++)
		   {
		     dataJJ[l]+=Lor(omega[l], eta, diff)*std::exp(-E(j)/T)*(JJ(i,j)*JJ(j,i)).real();
		     dataJeJe[l]+=Lor(omega[l], eta, diff)*std::exp(-E(j)/T)*(JeJe(i,j)*JeJe(j,i)).real();
}
       }       
}
 for(int l=0; l<omega.size(); l++)
		   {
		     dataJJ[l]/=Z;
		       dataJeJe[l]/=Z;
}		  
 ToFile(dataJJ,dirname+"/opc_"+params+".dat", dataJJ.size());
 ToFile(dataJeJe,dirname+"/kap_"+params+".dat", dataJeJe.size());
 ToFile(omega, dirname+"/omg_"+params+".dat", omega.size());
   	    


  return 0;
}
 
