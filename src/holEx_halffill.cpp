#include<iostream>
#define EIGEN_USE_MKL_ALL
#include"basis.hpp"
#include"operators.hpp"
#include"diag.h"
#include"thermal_edops.hpp"
#include"files.hpp"
#include"timeev.hpp"
#include"ETH.hpp"
#include"tpoperators.hpp"
#include <boost/program_options.hpp>
int main(int argc, char *argv[])
{
  using namespace Eigen;
using namespace std;
using namespace Many_Body;
  using Fermi_HubbardBasis= TensorProduct<ElectronBasis, ElectronBasis>;
   using HolsteinBasis= TensorProduct<ElectronBasis, PhononBasis>;
  using Mat= Operators::Mat;
  using boost::program_options::value;
   size_t M{};
  size_t L{};
  int Ne{};
  double t0{};
  double omega{};
  double gamma{};

  bool PB{};
  std::string dirname="";
  std::string filename={};
  
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("L", value(&L)->default_value(4), "L")
      ("M,m", value(&M)->default_value(2), "M")
      ("Ne", value(&Ne)->default_value(1), "ne")
      ("t0", value(&t0)->default_value(1.), "t0")
      ("gam", value(&gamma)->default_value(1.), "gamma")
      ("omg", value(&omega)->default_value(1.), "omega")
      ("dir", value(&dirname)->default_value(""), "dirname")
    ("pb", value(&PB)->default_value(true), "PB");
  


    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);

  if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("L"))
      {      std::cout << "L: " << vm["L"].as<size_t>() << '\n';
	filename+="L"+std::to_string(vm["L"].as<size_t>());
      }
     if (vm.count("M"))
      {
	std::cout << "M: " << vm["M"].as<size_t>() << '\n';
	filename+="M"+std::to_string(vm["M"].as<size_t>());
      }
         if (vm.count("Ne"))
      {      std::cout << "Ne: " << vm["Ne"].as<int>() << '\n';


      }

      if (vm.count("t0"))
      {
	std::cout << "t0: " << vm["t0"].as<double>() << '\n';
	filename+="t0"+std::to_string(vm["t0"].as<double>()).substr(0, 6);
      }
       if (vm.count("omg"))
      {
	std::cout << "omega: " << vm["omg"].as<double>() << '\n';
	filename+="omega"+std::to_string(vm["omg"].as<double>()).substr(0, 6);
      }

       if (vm.count("gam"))
      {
	std::cout << "gamma: " << vm["gam"].as<double>() << '\n';
	filename+="gam"+std::to_string(vm["gam"].as<double>()).substr(0, 6);
      }
       if (vm.count("pb"))
      {
	std::cout << "PB: " << vm["pb"].as<bool>() << '\n';
	filename+="PB"+std::to_string(vm["pb"].as<bool>());
      }
    }
  }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }




  ElectronBasis e( L, Ne);

  PhononBasis ph(L, M);
  //         std::cout<< ph<<std::endl;
      HolsteinBasis TP(e, ph);
 
 		filename+=".bin";
	   


 		std::cout<< "dim "<< TP.dim << std::endl;  


 		Mat E1=Operators::EKinOperatorL(TP, e, t0, PB);

 		Mat Ebdag=Operators::NBosonCOperator(TP, ph, gamma, PB);
 		Mat Eb=Operators::NBosonDOperator(TP, ph, gamma, PB);
 		Mat Eph=Operators::NumberOperator(TP, ph, omega,  PB);

		//-gam sun x_i /2
		double C=1;
		Mat Eextra=-gamma*0.5*((Operators::BosonDOperator_1(TP, ph,C,0,PB))+(Operators::BosonCOperator_1(TP, ph,C,0,PB)));
		for(int i=1; i<L;i++)
		  {
		    Eextra+=-gamma*0.5*((Operators::BosonDOperator_1(TP, ph,C,i,PB))+(Operators::BosonCOperator_1(TP, ph,C,i,PB)));
		  }
	 Eigen::MatrixXd A=Eigen::MatrixXd::Zero(TP.dim, TP.dim);      
	 Eigen::MatrixXd A_c=Eigen::MatrixXd::Zero(TP.dim, TP.dim);      

    Mat Cur_E(TP.dim, TP.dim);

    for(int i=0; i<L-1; i++)
      {

	Cur_E+=Operators::CurCoupOperator_loc(TP, e,ph,i,i+1, -t0*gamma);
	A+=(i)*(Operators::EKinLocalOperator(TP, e,i,i+1, t0)+Operators::NumberOperatorph_1(TP, ph,omega, i,PB));
	A+=(i)*(Eigen::MatrixXd(Operators::Elecnr_localOperator(TP, e,i,gamma))*(Eigen::MatrixXd(Operators::BosonDOperator_1(TP, ph,C,i,PB))+Eigen::MatrixXd(Operators::BosonCOperator_1(TP, ph,C,i,PB))));
	A_c+=(i)*(Eigen::MatrixXd(Operators::Elecnr_localOperator(TP, e,i,C)));
      }
    A+=(L-1)*(Operators::NumberOperatorph_1(TP, ph,omega, L-1,PB));
 A+=(L-1)*(Eigen::MatrixXd(Operators::Elecnr_localOperator(TP, e,L-1,gamma))*(Eigen::MatrixXd(Operators::BosonDOperator_1(TP, ph,C,L-1,PB))+Eigen::MatrixXd(Operators::BosonCOperator_1(TP, ph,C,L-1,PB))));

 A_c+=(L-1)*(Eigen::MatrixXd(Operators::Elecnr_localOperator(TP, e,L-1,C)));
for(int i=1; i<L-1; i++)
      {
Cur_E+=Operators::CurrLocalOperator(TP, e,i-1,i+1, t0*t0);

      }
 if(PB)
   {
	Cur_E+=Operators::CurCoupOperator_loc(TP, e,ph,L-1,0, -t0*gamma);

	Cur_E+=Operators::CurrLocalOperator(TP, e,L-2,0, t0*t0);
Cur_E+=Operators::CurrLocalOperator(TP, e,L-1,1, t0*t0);
 A+=(L-1)*(Operators::EKinLocalOperator(TP, e,L-1,0, t0));
}



       Mat H=E1+Eph  +Ebdag + Eb+Eextra;
            Eigen::MatrixXcd Cur_Ecp=(Cur_E);
	    Cur_Ecp*=-std::complex<double>(0,1);  	   


   auto J=Operators::CurOperatorL(TP, e, t0, PB);    

 
       Eigen::VectorXd eigenVals(TP.dim);

    
       Eigen::MatrixXd HH=Eigen::MatrixXd(H);
          Eigen::MatrixXd JJ=Eigen::MatrixXd(J);

 	  Eigen::MatrixXcd JJcp=JJ*std::complex<double>(0,1);
     // 	  std::cout<< A_c*HH-HH*A_c<<std::endl;
     // std::cout<<"comm ex "<<endl;



     // std::cout<<"A "<<endl;
     // 	  std::cout<< A<<std::endl;
     // 	  std::cout<<std::endl;
     // 	  std::cout<< A_c<<std::endl;
     // std::cout<<"kin "<<endl;

     // 	  std::cout<< E1<<std::endl;
     // 	  std::cout<<endl;
     // std::cout<<"JS "<<endl;
     // 	  std::cout<< JJcp<<std::endl;
     // 	  std::cout<<endl;
     // 	  std::cout<< Cur_Ecp<<std::endl;
              Many_Body::diagMat(HH, eigenVals);
	   Eigen::MatrixXd EKIN=HH.adjoint()*E1*HH;


	      Eigen::MatrixXcd HH_cp=HH.cast<std::complex<double>>();

     Eigen::MatrixXcd COMM_Je=(A*Cur_Ecp-Cur_Ecp*A);
     Eigen::MatrixXcd COMM_J=(A_c*JJcp-JJcp*A_c); 
     // std::cout<<"commy "<<endl;
     // 	  std::cout<< COMM_J<<std::endl;
     // 	  std::cout<<std::endl;
     // 	  std::cout<< COMM_Je<<std::endl;
   Eigen::MatrixXcd JJmat=HH_cp.adjoint()*(JJcp*HH_cp);
   Eigen::MatrixXcd JeJemat=HH_cp.adjoint()*(Cur_Ecp*HH_cp);

   Eigen::MatrixXcd comm_Jemat=HH_cp.adjoint()*(COMM_Je*HH_cp);
   Eigen::MatrixXcd comm_Jmat=HH_cp.adjoint()*(COMM_J*HH_cp);


     // std::cout<<"comms "<<endl;
     // 	  std::cout<< EKIN<<std::endl;
     // std::cout<<"comms "<<endl;
     // 	  std::cout<< comm_Jmat<<std::endl;
     // 	  std::cout<<endl;
     // 	  std::cout<< comm_Jemat<<std::endl;


   std::cout<< JJmat.cols()<< " and "<< JJmat.rows()<<std::endl;
  


       	   bin_write(dirname+"/comm_Jmat.bin", comm_Jmat);
       	   bin_write(dirname+"/comm_Jemat.bin", comm_Jemat);

       	   bin_write(dirname+"/JJmat.bin", JJmat);
       	   bin_write(dirname+"/JeJemat.bin", JeJemat);


   	     bin_write(dirname+"/E.bin", eigenVals);
       Eigen::VectorXd EKdiag=EKIN.diagonal();
       //       std::cout<<EKdiag<<std::endl;
   	     bin_write(dirname+"/EKdiag.bin", EKdiag);
    

  return 0;
}
 
