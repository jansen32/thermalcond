LIBRARY_DIR=$(ITENSOR)
include $(LIBRARY_DIR)/this_dir.mk
include $(LIBRARY_DIR)/options.mk
INC+=-I${ITENSOR}
INC+=-I${TENSORFT}/include/
INC+=-I${TENSORTIMEEV}/include/
INC+=-I${TENSOR}/TDVP/
INC+=-I${TENSOR}/parallel_tdvp_lbo
INC+=-I$(PWD)/include
INC+=-I${EIGEN}
INC+=-I${EINC}


LIBSLINK+=-L${MANYBODY}/libs
LIBSLINK+=-L${ELIBS}
LIBSPATH=-L$(ITENSOR)/lib
LIBSPATH+=$(LIBSLINK)






#################################
# ED code 
###############################
INC_ED=-I${MANYBODY}/include
INC_ED+=-I${EIGEN}
INC_ED+=-I${EINC}
INC_ED+=-I$(PWD)/include
LIBSLINK_ED=-L${MANYBODY}/libs -L${ELIBS}
LIBS_ED=-leigenmkl32 -lboost_program_options
MKLLINK_ED=-DMKL_Complex16="std::complex<double>" -m64 -I${MKLROOT}/include -L${MKLROOT}/lib/intel64 -Wl,--no-as-needed -lmkl_intel_lp64 -lmkl_gnu_thread -lmkl_core -lgomp -lpthread -lm -ldl

#########################

CCFLAGS+=-I. $(ITENSOR_INCLUDEFLAGS) $(OPTIMIZATIONS) -Wno-unused-variable -std=c++17 -O2 -std=gnu++1z
CCGFLAGS+=-I. $(ITENSOR_INCLUDEFLAGS) $(DEBUGFLAGS) 

LIBFLAGS=-L$(ITENSOR_LIBDIR) $(ITENSOR_LIBFLAGS) -lboost_program_options -lboost_filesystem 
LIBGFLAGS=-L$(ITENSOR_LIBDIR) $(ITENSOR_LIBGFLAGS) -lboost_program_options -lboost_filesystem 
#LIBFLAGS=-L$(ITENSOR_LIBDIR) $(ITENSOR_LIBFLAGS) -Wl,--start-group /share/scratch1/jansen32/usr/lib2/lib/libboost_program_options.a /share/scratch1/jansen32/usr/lib2/lib/libboost_filesystem.a -Wl,--end-group
#LIBGFLAGS=-L$(ITENSOR_LIBDIR) $(ITENSOR_LIBGFLAGS) -Wl,--start-group /share/scratch1/jansen32/usr/lib2/lib/libboost_program_options.a /share/scratch1/jansen32/usr/lib2/lib/libboost_filesystem.a -Wl,--end-group
MPICOM=mpic++ -m64 -std=c++17 -fconcepts -fPIC



CPPFLAGS_EXTRA += -O2 -std=c++17
#MPILINK= -lboost_serialization -lboost_mpi  -I$MPI_INCLUDE -L$MPI_LIB 


DB=-g
CXX=g++
ND=-DNDEBUG
OP+=-O2
#-fopenmp -DOMPPAR

#
#BINS=


##################################################


holFTtdvp_lbo_ts: src/holFTtdvp_lbo_ts.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)
holFTtdvp_lbo_ts_halffill: src/holFTtdvp_lbo_ts_halffill.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)
holFTtdvp_lbo_ts_staggered: src/holFTtdvp_lbo_ts_staggered.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)
holFTtdvp_lbo_ts_allLBOdims: src/holFTtdvp_lbo_ts_allLBOdims.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)

################
#GS############
################

JJGStdvpOneSite: src/JJGStdvpOneSite.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)
########################





JJFTtdvpOneSite: src/JJFTtdvpOneSite.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)
JJFTtdvpOneSite_halffill: src/JJFTtdvpOneSite_halffill.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)
JJFTtdvpOneSite_staggered: src/JJFTtdvpOneSite_staggered.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)
JJFTtdvplboOneSite: src/JJFTtdvplboOneSite.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)
JeJeFTtdvpOneSite: src/JeJeFTtdvpOneSite.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)
JeJeFTtdvpOneSite_halffill: src/JeJeFTtdvpOneSite_halffill.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)
JeJeFTtdvpOneSite_staggered: src/JeJeFTtdvpOneSite_staggered.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)

JeJeREDFTtdvpOneSite: src/JeJeREDFTtdvpOneSite.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)
JeJeREDFTtdvpOneSite_halffill: src/JeJeREDFTtdvpOneSite_halffill.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)
JeJeREDFTtdvpOneSite_staggered: src/JeJeREDFTtdvpOneSite_staggered.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)
JeJePHFTtdvpOneSite: src/JeJePHFTtdvpOneSite.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)
print_state: src/print_state.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)

holGS: src/holGS.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)

FT_test: src/FT_test.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)
FT_overlapcurrents: src/FT_overlapcurrents.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)


###############################
# ED
holstFTexact: src/holstFTexact.cpp
	$(CXX) $(FLAGS) $(INC_ED) $< -o bin/$@  $(MKLLINK_ED) $(LIBS_ED) $(LIBS_ED) $(LIBSLINK_ED) $(ND) $(OP)

holEx_expvalGC: src/holEx_expvalGC.cpp
	$(CXX) $(FLAGS) $(INC_ED) $< -o bin/$@  $(MKLLINK_ED) $(LIBS_ED) $(LIBS_ED) $(LIBSLINK_ED) $(ND) $(OP)

holstexactED: src/holstexactED.cpp
	$(CXX) $(FLAGS) $(INC_ED) $< -o bin/$@  $(MKLLINK_ED) $(LIBS_ED) $(LIBS_ED) $(LIBSLINK_ED) $(ND) $(OP)

JJsHolExFT_timeev: src/JJsHolExFT_timeev.cpp
	$(CXX) $(FLAGS) $(INC_ED) $< -o bin/$@  $(MKLLINK_ED) $(LIBS_ED) $(LIBS_ED) $(LIBSLINK_ED) $(ND) $(OP)
JJsHolExFT_timeev_halffill: src/JJsHolExFT_timeev_halffill.cpp
	$(CXX) $(FLAGS) $(INC_ED) $< -o bin/$@  $(MKLLINK_ED) $(LIBS_ED) $(LIBS_ED) $(LIBSLINK_ED) $(ND) $(OP)
holEx_halffill: src/holEx_halffill.cpp
	$(CXX) $(FLAGS) $(INC_ED) $< -o bin/$@  $(MKLLINK_ED) $(LIBS_ED) $(LIBS_ED) $(LIBSLINK_ED) $(ND) $(OP)
holExDrude_halffill: src/holExDrude_halffill.cpp
	$(CXX) $(FLAGS) $(INC_ED) $< -o bin/$@  $(MKLLINK_ED) $(LIBS_ED) $(LIBS_ED) $(LIBSLINK_ED) $(ND) $(OP)
Ex_transportcoeffs: src/Ex_transportcoeffs.cpp
	$(CXX) $(FLAGS) $(INC_ED) $< -o bin/$@  $(MKLLINK_ED) $(LIBS_ED) $(LIBS_ED) $(LIBSLINK_ED) $(ND) $(OP)

###############
# parallell programs
JJFTpara_timeContsecimpl: src/JJFTpara_timeContsecimpl.cpp $(ITENSOR_LIBS) 
	$(MPICOM) -std=c++17 $<  -o bin/$@  $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)
JeJeFTpara_timeContsecimpl: src/JeJeFTpara_timeContsecimpl.cpp $(ITENSOR_LIBS) 
	$(MPICOM) -std=c++17 $<  -o bin/$@  $(CCFLAGS) $(INC)  $(LIBSPATH) $(LIBFLAGS)  $(ND)

########################################

clean:
	rm bin/*

