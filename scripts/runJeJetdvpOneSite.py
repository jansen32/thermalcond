#!/usr/bin/python
import sys
import numpy as np
import os
import subprocess
import shlex

exe="JeJeFTtdvpOneSite"


L=15
M=20
n_e=5
t0=1.0
gam=1.0
#=np.sqrt(2.)
#gams=3.0
#np.sqrt(1.6)


omg=1.0
omgp="0.000"
dt=0.01
tot=20.



cut=1e-08

Maxd=3000
Mind_state=0
Mind_sim=0


T="0.20"

state_dname="holFTtdvp_lbo_tsL{0}M{1}ne{2}t0{3:.3f}gam{4:.3f}omg{5:.3f}omgp{6}dt0.10000T0.100cut1e-09lbocut1e-09Mind{8}Maxd{7}DIR/STATES/".format(L,M,n_e,t0,gam,omg,omgp, Maxd, Mind_state)
nam="aT{0}FTdisptdvplboL{1}M{2}n_e{3}t01.000omg{6:.3f}omgp{4}gam{5}T0.100dt0.1000cut1e-09Maxd{7}Mind{8}lboMd100lbocut1e-09.bin".format(T,L,M,n_e,omgp, "{0:.5f}".format(gam)[0:5],omg, Maxd, Mind_state)
mpsN="MPS"+nam
siteN="siteset"+nam
dirname="{0}dt{1:.4f}tot{2:.3f}cut{3}MinD{5}maxD{6}{4}DIR".format(exe, dt,tot, cut, mpsN[:-4],Mind_sim, Maxd)





if(os.path.isdir(dirname)):
    dirname+="2"

if(os.path.isdir(dirname)):
    dirname+="2"
if(os.path.isdir(dirname)):
    print("to many versions of dirName exist")
    sys.exit()
os.mkdir(dirname)
comm="time MKL_THREADING_LAYER=sequential OMP_NUM_THREADS=2 ./bin/{0} --d {1} --L {2} --M {3}  --t0 {4} --gam {5} --omg {6} --omgp {7} --dt {8} --tot {9} --cut {10}  --Maxd {11} --Mind {15}  --mpsN {14}/{12} --siteN {14}/{13} | tee {1}/log.txt".format(exe,dirname, L,M,t0,gam,omg,omgp,dt,tot,cut,Maxd, mpsN, siteN, state_dname, Mind_sim)
print(comm)
comm_file=open(dirname+"/command.txt","w")
comm_file.write(comm)
comm_file.close()
stdout=subprocess.check_call(comm, shell=True, stdout=sys.stdout, stderr=sys.stderr)


