#!/usr/bin/python
import sys
import numpy as np
import os
import subprocess
import shlex

exe="JJGStdvpOneSite"


L=22
M=20
n_e=int(L/2)
#int(L/2)
t0=1.0
gam=1.0
#np.sqrt(1.6)
#np.sqrt(2)
#



omg=1.0
omgp="0.000"
dt=0.01
tot=20.

cut=1e-08


Maxd=3000
Mind_state=0
Mind_sim=0





nam="GSdmrgL{0}M{1}N{2}MS1000t01.000gam{3}omega{4:.3f}eps0.000.bin".format(L,M,n_e,"{0:.5f}".format(gam)[0:5],omg)

mpsN="MPS"+nam
siteN="siteset"+nam
dirname="{0}dt{1:.4f}tot{2:.3f}cut{3}MinD{5}maxD{6}{4}DIR".format(exe, dt,tot, cut, mpsN[:-4],Mind_sim, Maxd)






if(os.path.isdir(dirname)):
    dirname+="2"

if(os.path.isdir(dirname)):
    dirname+="2"
if(os.path.isdir(dirname)):
    print("to many versions of dirName exist")
    sys.exit()
os.mkdir(dirname)
comm="time MKL_THREADING_LAYER=sequential OMP_NUM_THREADS=2 ./bin/{0} --d {1} --L {2} --M {3}  --t0 {4} --gam {5} --omg {6}  --dt {7} --tot {8} --cut {9}  --Maxd {10} --Mind {13}  --mpsN {11} --siteN {12} | tee {1}/log.txt".format(exe,dirname, L,M,t0,gam,omg,dt,tot,cut,Maxd, mpsN, siteN,  Mind_sim)
print(comm)
comm_file=open(dirname+"/command.txt","w")
comm_file.write(comm)
comm_file.close()
stdout=subprocess.check_call(comm, shell=True, stdout=sys.stdout, stderr=sys.stderr)


