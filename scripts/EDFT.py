#!/usr/bin/python
import sys
import numpy as np
import os
import subprocess
L=3
gam=np.sqrt(1.6)


omg=0.400





t0=1.

M=1

Ne=1
pb=0
exe="holstFTexact"
dirName="EDDATA/{0}L{1}Ne{2}M{3}t0{4:.3f}gam{5:.3f}omg{6:.3f}pb{7}DIR".format(exe, L,Ne, M, t0, gam, omg,  pb)


print(dirName)
if(os.path.isdir(dirName)):
    dirName+="2"
if(os.path.isdir(dirName)):
    dirName+="2"
if(os.path.isdir(dirName)):
    print("to many versions of dirName exist")
    sys.exit()
os.mkdir(dirName)

comm="time OMP_NUM_THREADS=4   ./bin/{0} --L {1} --Ne {2} --M {3} --t0 {4} --gam {5} --omg {6}  --d {7} --pb {8}".format(exe, L,Ne, M, t0, gam, omg,  dirName, pb)

comm_file=open(dirName+"/command.txt","w")
comm_file.write(comm)
comm_file.close()
stdout=subprocess.check_call(comm, shell=True, stdout=sys.stdout, stderr=sys.stderr)
