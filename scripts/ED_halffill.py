#!/usr/bin/python
import sys
import numpy as np
import os
import subprocess
L=14
gam=0.0
#np.sqrt(1.6)


omg=0.0
omgp=0.000



Ne=int(L/2)
t0=1.

M=0
tot=100.
dt=0.01
pb=0
exe="holEx_halffill"
dirName="EDDATA/{0}L{1}M{2}Ne{7}t0{3:.3f}gam{4:.3f}omg{5:.3f}pb{6}DIR".format(exe, L, M, t0, gam, omg,  pb,  Ne)


print(dirName)
if(os.path.isdir(dirName)):
    dirName+="2"
if(os.path.isdir(dirName)):
    dirName+="2"
if(os.path.isdir(dirName)):
    print("to many versions of dirName exist")
    sys.exit()
os.mkdir(dirName)

comm="time OMP_NUM_THREADS=4   ./bin/{0} --L {1} --M {2} --Ne {8} --t0 {3} --gam {4} --omg {5} --dir {6} --pb {7}  | tee {6}/log.txt".format(exe, L, M, t0, gam, omg,  dirName, pb,  Ne)

comm_file=open(dirName+"/command.txt","w")
comm_file.write(comm)
comm_file.close()
stdout=subprocess.check_call(comm, shell=True, stdout=sys.stdout, stderr=sys.stderr)
#
