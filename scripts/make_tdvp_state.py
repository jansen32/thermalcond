
#!/usr/bin/python
import sys
import numpy as np
import os
import subprocess
import shlex

exe="holFTtdvp_lbo_ts"

L=15
M=20

ne=5
#int((L+1)/2)

t0=1.0
gam=1.0
#np.sqrt(2)
#np.sqrt(1.6)
omg=1.0
omgp=0.0
dt=0.1
T=0.1
cut=1e-09
lbocut=1e-09
Maxd=3000
Mind=0



dirname="{0}L{1}M{2}ne{3}t0{4:.3f}gam{5:.3f}omg{6:.3f}omgp{7:.3f}dt{8:.5f}T{9:.3f}cut{10}lbocut{11}Mind{12}Maxd{13}DIR".format(exe, L,M,ne,t0,gam,omg,omgp,dt,T,cut,lbocut,Mind, Maxd)



if(os.path.isdir(dirname)):
    dirname+="2"

if(os.path.isdir(dirname)):
    dirname+="2"
if(os.path.isdir(dirname)):
    print("to many versions of dirName exist")
    sys.exit()
os.mkdir(dirname)
os.mkdir(dirname+"/DATA/")
os.mkdir(dirname+"/STATES/")
os.mkdir(dirname+"/LOGFILE/")
comm="time MKL_THREADING_LAYER=sequential OMP_NUM_THREADS=4 ./bin/{0} --d {1} --L {2} --M {3} --n_e {4} --t0 {5} --gam {6} --omg {7} --omgp {8} --dt {9} --T {10} --cut {11} --lbocut {12} --Maxd {13} --Mind {14}  --SA 1 | tee {1}/LOGFILE/log.txt".format(exe,dirname, L,M,ne,t0,gam,omg,omgp,dt,T,cut,lbocut,Maxd, Mind) #--Mind {14}
comm_file=open(dirname+"/LOGFILE/command.txt","w")
comm_file.write(comm)
comm_file.close()
stdout=subprocess.check_call(comm, shell=True, stdout=sys.stdout, stderr=sys.stderr)


