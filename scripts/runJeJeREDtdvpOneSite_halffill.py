
#!/usr/bin/python
import sys
import numpy as np
import os
import subprocess
import shlex

exe="JeJeREDFTtdvpOneSite_halffill"

L=22
M=20
n_e=int(L/2)
t0=1.0
gam=1.0
#np.sqrt(1.6)
omg=1.0

dt=0.01
tot=20.

cut=1e-07

Maxd=3000
Maxd_sim=3000
Mind_state=0
Mind_sim=0

T="0.22"
state_dname="holFTtdvp_lbo_ts_halffillL{0}M{1}ne{2}t0{3:.3f}gam{4:.3f}omg{5:.3f}dt0.10000T0.100cut1e-09lbocut1e-09Mind{7}Maxd{6}DIR2/STATES/".format(L,M,n_e,t0,gam,omg, Maxd, Mind_state)
nam="aT{0}FTdisptdvplboL{1}M{2}n_e{3}t01.000omg{5:.3f}gam{4}T0.100dt0.1000cut1e-09Maxd{6}Mind{7}lboMd100lbocut1e-09.bin".format(T,L,M,n_e,"{0:.5f}".format(gam)[0:5],omg, Maxd, Mind_state)

mpsN="MPS"+nam
siteN="siteset"+nam
dirname="{0}dt{1:.4f}tot{2:.3f}cut{3}MinD{5}maxD{6}{4}DIR".format(exe, dt,tot, cut, mpsN[:-4],Mind_sim, Maxd_sim)






if(os.path.isdir(dirname)):
    dirname+="2"

if(os.path.isdir(dirname)):
    dirname+="2"
if(os.path.isdir(dirname)):
    print("to many versions of dirName exist")
    sys.exit()
os.mkdir(dirname)
comm="time MKL_THREADING_LAYER=sequential OMP_NUM_THREADS=2 ./bin/{0} --d {1} --L {2} --M {3}  --t0 {4} --gam {5} --omg {6}  --dt {7} --tot {8} --cut {9}  --Maxd {10} --Mind {14}  --mpsN {13}/{11} --siteN {13}/{12} | tee {1}/log.txt".format(exe,dirname, L,M,t0,gam,omg,dt,tot,cut,Maxd_sim, mpsN, siteN, state_dname, Mind_sim)
print(comm)
comm_file=open(dirname+"/command.txt","w")
comm_file.write(comm)
comm_file.close()
stdout=subprocess.check_call(comm, shell=True, stdout=sys.stdout, stderr=sys.stderr)


