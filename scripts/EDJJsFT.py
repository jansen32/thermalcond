#!/usr/bin/python
import sys
import numpy as np
import os
import subprocess
L=2
gam=3.
#np.sqrt(2)


omg=1.0
omgp=0.000
T=1.0


Ne=1
t0=1.

M=40
tot=100.
dt=0.01
pb=0
exe="JJsHolExFT_timeev"
dirName="EDDATA/{0}aT{10:.3f}L{1}M{2}Ne{11}t0{3:.3f}gam{4:.3f}omg{5:.3f}omgp{6:.3f}dt{7:.3f}tot{8:.3f}pb{9}DIR".format(exe, L, M, t0, gam, omg, omgp, dt, tot, pb, T, Ne)


print(dirName)
if(os.path.isdir(dirName)):
    dirName+="2"
if(os.path.isdir(dirName)):
    dirName+="2"
if(os.path.isdir(dirName)):
    print("to many versions of dirName exist")
    sys.exit()
os.mkdir(dirName)

comm="time OMP_NUM_THREADS=4   ./bin/{0} --L {1} --M {2} --Ne {12} --t0 {3} --gam {4} --omg {5} --omgp {6} --dt {7} --tot {8} --dir {9} --pb {10} --T {11} | tee {9}/log.txt".format(exe, L, M, t0, gam, omg, omgp, dt, tot, dirName, pb, T, Ne)

comm_file=open(dirName+"/command.txt","w")
comm_file.write(comm)
comm_file.close()
stdout=subprocess.check_call(comm, shell=True, stdout=sys.stdout, stderr=sys.stderr)
#
