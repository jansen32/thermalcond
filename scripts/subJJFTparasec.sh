#!/bin/bash
#SBATCH -p medium
#SBATCH -N 1
#SBATCH -n 8
#SBATCH --mem-per-cpu=2G
#SBATCH -t 40:00:00
#SBATCH -o out.%J
#SBATCH -e err.%j
#SBATCH --cpus-per-task=4
#SBATCH --mail-type=ALL
#SBATCH --mail-user=david.jansen@uni-goettingen.de
#SBATCH -C scratch

module load gcc/9.3.0                          
module load anaconda3/2021.05
module load intel-parallel-studio/cluster.2020.4


module load openmpi/4.1.1
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/users/jansen32/usr/lib
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export MKL_THREADING_LAYER=sequential
prcs=$SLURM_NTASKS

M="20"
L=20
ne=5
lboMd="100"
cut="1e-09"
lbocut="1e-09"

md=2000
#export UCX_LOG_LEVEL=error
gam=1.0
#$(awk -v x=2. 'BEGIN{print sqrt(x)}')
gams="1.000"
T="0.20"
omg="1.000"
omgp="0.100"
dt="0.01"
statedir="/scratch/users/$USER/holFTtdvp_lbo_tsL15M15ne5t01.000gam0.500omg1.000omgp0.000dt0.10000T0.100cut1e-09lbocut1e-09Mind0Maxd3000DIR/STATES/"
name="aT${T}FTdisptdvplboL${L}M${M}n_e${ne}t01.000omg1.000omgp0.000gam${gams}T0.100dt0.1000cut1e-09Maxd3000Mind0lboMd100lbocut1e-09.bin"
mpsN="${statedir}/MPS${name}"
siteN="${statedir}/siteset${name}"


exe=JJFTpara_timeContsecimpl

tin=0.0000
tot=6.0000
dn="JJFT"
dirname="/scratch/users/$USER/${dn}/${exe}tot${tot}tin${tin}dt${dt}prcs${prcs}cut${cut}lbocut${lbocut}Md${md}MPSaT${T}FTdisptdvplboL${L}M${M}n_e${ne}t01.000omg1.000omgp0.000gam${gams}T0.100dt0.1000cut1e-09Maxd3000Mind0lboMd100lbocut1e-09DIR"


if [ -d "$dirname" ]; then
  # Take action if $DIR exists. #
  echo "Installing config files in ${DIR}..."
  dirname="/scratch/users/$USER/${dn}/${exe}tot${tot}tin${tin}dt${dt}prcs${prcs}cut${cut}lbocut${lbocut}Md${md}MPSaT${T}FTdisptdvplboL${L}M${M}n_e${ne}t01.000omg1.000omgp0.000gam${gams}T0.100dt0.1000cut1e-09Maxd3000Mind0lboMd100lbocut1e-09DIR2"
fi
echo "hei"
mkdir ${dirname}
comm="mpirun -n ${prcs} ./bin/${exe} --lbocut ${lbocut} --cut ${cut} --mpsNK ${mpsN} --mpsNB ${mpsN} --dt ${dt} --siteN ${siteN}  --tot ${tot}  --L ${L} --gam ${gam} --M $M --omg ${omg} --omgp ${omgp} --appOP 1 --t_in ${tin} --d ${dirname} --Md ${md}"

echo ${comm} >${dirname}/commando.txt
echo $SLURM_JOB_ID >${dirname}/jobid.txt
eval ${comm}




#mpirun -n 10 ./bin/${exe}
