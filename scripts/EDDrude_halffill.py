#!/usr/bin/python
import sys
import numpy as np
import os
import subprocess
L=6
gam=0.
#np.sqrt(1.6)


omg=0.
omgp=0.000



Ne=1
t0=1.

M=0

dt=0.01
pb=1
exe_old="holEx_halffill"
exe="holExDrude_halffill"
dirName="EDDATA/{0}L{1}M{2}Ne{7}t0{3:.3f}gam{4:.3f}omg{5:.3f}pb{6}DIR".format(exe_old, L, M, t0, gam, omg,  pb,  Ne)


comm="time OMP_NUM_THREADS=4   ./bin/{0} --L {1} --M {2} --Ne {3} --dir {4}  | tee {4}/Drudeweight.txt".format(exe, L, M, Ne,  dirName)

comm_file=open(dirName+"/command.txt","w")
comm_file.write(comm)
comm_file.close()
stdout=subprocess.check_call(comm, shell=True, stdout=sys.stdout, stderr=sys.stderr)
#
