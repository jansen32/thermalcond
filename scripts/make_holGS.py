#!/usr/bin/python
import sys
import numpy as np
import os
import subprocess
import shlex

M="20"

L=22
N=int(L/2)


gam=1.0
#np.sqrt(1.6)
#gam=np.sqrt(13)*0.5
##
omg=1.0

t0=1.
eps=0
MS=1000

#int(L/2)
filename="GShol_2L{0}M{1}Ne{7}gam{2:.3f}omg{3}eps{4}t0{5}MS{6}.txt".format(L,M,gam, omg, eps, t0,MS, N)
comm="time MKL_THREADING_LAYER=sequential OMP_NUM_THREADS=4 ./bin/holGS --L {0}  --M {1} --gam {2} --omg {3} --eps {4}  --t0 {5}  --MS {6} --N {8} --SS 1 | tee {7}".format(L,M,gam, omg, eps, t0,MS,filename, N )



comm_list=shlex.split(comm)
#print(comm_list)

#['echo', 'More output'],
#process = subprocess.Popen(comm_list,shell=True,
 #                    stdout=subprocess.PIPE, 
  #                   stderr=subprocess.PIPE)
stdout=subprocess.check_call(comm, shell=True, stdout=sys.stdout)

