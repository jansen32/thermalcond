#!/usr/bin/python
import sys
import numpy as np
import os
import subprocess
L=6
gam=1.
#np.sqrt(1.6)


omg=1.




Ne=3
t0=1.
T=0.1
eta=0.01
M=1


pb=0
om_max=14
dom=0.1
params='T{0:.3f}eta{1:.3f}dom{2:.3f}om_max{3:.3f}'.format(T,eta, dom, om_max)
exe_old="holEx_halffill"
exe="Ex_transportcoeffs"
dirName="EDDATA/{0}L{1}M{2}Ne{7}t0{3:.3f}gam{4:.3f}omg{5:.3f}pb{6}DIR".format(exe_old, L, M, t0, gam, omg,  pb,  Ne)


comm="time OMP_NUM_THREADS=4   ./bin/{0} --L {1} --M {2} --Ne {3} --dir {4} --eta {5} --dom {6} --om_max {7} --T {8} --params {9}".format(exe, L, M, Ne,  dirName, eta, dom,om_max, T, params )

comm_file=open(dirName+"/command.txt","w")
comm_file.write(comm)
comm_file.close()
stdout=subprocess.check_call(comm, shell=True, stdout=sys.stdout, stderr=sys.stderr)
#
