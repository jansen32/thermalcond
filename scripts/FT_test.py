#!/usr/bin/python
import sys
import numpy as np
import os
import subprocess
import shlex

exe="FT_test"


L=10
M=40
n_e=int(L/2)
t0=1.0
gam=np.sqrt(1.6)
#np.sqrt(2)
#
T=sys.argv[1]


omg=0.4
omgp="0.000"
dt=0.01
tot=20.

cut=1e-09


Maxd=3000
Maxd_sim=5000
Mind_state=0
Mind_sim=0




state_dname="holFTtdvp_lbo_ts_halffillL{0}M{1}ne{2}t0{3:.3f}gam{4:.3f}omg{5:.3f}dt0.01000T0.100cut1e-09lbocut1e-09Mind{7}Maxd{6}DIR22/STATES/".format(L,M,n_e,t0,gam,omg, Maxd, Mind_state)
nam="aT{0}FTdisptdvplboL{1}M{2}n_e{3}t01.000omg{5:.3f}gam{4}T0.100dt0.0100cut1e-09Maxd{6}Mind{7}lboMd100lbocut1e-09.bin".format(T,L,M,n_e,"{0:.5f}".format(gam)[0:5],omg, Maxd, Mind_state)

mpsN="MPS"+nam
siteN="siteset"+nam

comm="time MKL_THREADING_LAYER=sequential OMP_NUM_THREADS=2 ./bin/{0}  --L {1} --M {2}  --t0 {3} --gam {4} --omg {5}   --cut {6}  --Maxd {7}  --mpsN {10}/{8} --siteN {10}/{9} | tee {0}cut{6}Md{7}_{11}.txt".format(exe, L,M,t0,gam,omg,cut,Maxd_sim, mpsN, siteN, state_dname, mpsN[:-4])
print(comm)
#comm_file=open(dirname+"/command.txt","w")
#comm_file.write(comm)
#comm_file.close()
stdout=subprocess.check_call(comm, shell=True, stdout=sys.stdout, stderr=sys.stderr)


